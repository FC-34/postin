postin is an open source post-installation scripting framework.

See doc/index.html for more information.

Latest version is available at https://gitlab.com/20-100-2fe/postin
