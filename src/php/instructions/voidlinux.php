<!DOCTYPE html>
<html>
	<head>
		<title>Void Linux installation procedure</title>
		<meta charset="UTF-8" />
		<style>
			ul > li {
				padding-bottom: 0.5em;
			}
		</style>
	</head>
	<body>
		<ul>
			<li>Download an <a href="http://alpha.de.repo.voidlinux.org/live/current/<?= $info->getHWInstallImage() ?>" 
			target="_blank">installer ISO</a> from <a href="http://alpha.de.repo.voidlinux.org/live/current/"
			target="_blank">Void's download site</a></li>
			<li>Save it to your VM host's ISO folder, or burn it onto an USB stick if you install on bare metal<br />
			<?php require_once 'burn-usb.php'; ?></li>
			<li>Install Void Linux <b>without GUI</b> (Postin will take care of it) and reboot</li>
			<li>Log in as root</li>
			<li>If you installed from media (rather than from network), update your system:<br/>
			<code>xbps-install -Suy</code><br/>
			<code>xbps-install -Suy</code> &nbsp; &nbsp; &nbsp; <small><i>(yes, twice.)</i></small></li>
			<li>Then reboot in order to use the updated kernel</li>
			<li>Log in as root</li>
			<li>Install wget:<br />
			<code>xbps-install -y wget</code></li>
		</ul>
	</body>
</html>
