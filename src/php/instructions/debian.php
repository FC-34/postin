<!DOCTYPE html>
<html>
	<head>
		<title>Debian installation procedure</title>
		<meta charset="UTF-8" />
		<style>
			ul > li {
				padding-bottom: 0.5em;
			}
		</style>
	</head>
	<body>
		<ul>
			<li>To install on <b>bare metal</b>, download a 
            <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/amd64/iso-cd/" 
			target="_blank">network installer ISO</a> <b>WITH non-free firmware</b> and burn it onto an USB stick<br />
			<?php require_once 'burn-usb.php'; ?></li>
			<li>To install a <b>virtual machine</b>, download a 
			<a href="https://cdimage.debian.org/cdimage/release/current/amd64/iso-cd/" target="_blank">network 
			installer ISO</a> <b>WITHOUT non-free firmware</b> and save it to your VM host's ISO folder.<br /> You can also test 
			a daily snapshot of "<a href="https://cdimage.debian.org/cdimage/daily-builds/daily/current/amd64/iso-cd/" 
			target="_blank">testing</a>".</li>
			<li>Install Debian <b>without GUI and print server</b> (Postin will take care of it) and reboot<br />
			Note: make sure to <b>unselect</b> the Desktop Environment choice from the installer's tasks default selection</li>
			<li>Log in as root</li>
		</ul>
	</body>
</html>
