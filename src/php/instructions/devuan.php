<!DOCTYPE html>
<html>
	<head>
		<title>Devuan installation procedure</title>
		<meta charset="UTF-8" />
		<style>
			ul > li {
				padding-bottom: 0.5em;
			}
		</style>
	</head>
	<body>
		<ul>
			<li>Download a <a href="http://devuan.c3l.lu/devuan_<?= $info->getVersionName() ?>/installer-iso/<?= $info->getHWInstallImage() ?>">network 
			installer ISO</a> from a <a href="https://devuan.org/get-devuan" target="_blank">Devuan mirror</a></li>
			<li>Save it to your VM host's ISO folder, or burn it onto an USB stick if you install on bare metal<br />
			<?php require_once 'burn-usb.php'; ?></li>
			<li>Install Devuan <b>without GUI and print server</b> (Postin will take care of it) and reboot<br />
			Note: make sure to <b>unselect</b> the Desktop Environment choice from the installer's tasks default selection</li>
			<li>Log in as root</li>
		</ul>
	</body>
</html>
