<!DOCTYPE html>
<html>
	<head>
		<title>DragonFlyBSD installation procedure</title>
		<meta charset="UTF-8" />
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Expires" content="0" />
		<meta http-equiv="Pragma" content="no-cache" />
		<style>
			ul > li {
				padding-bottom: 0.5em;
			}
		</style>
	</head>
	<body>
		<ul>
			<li>To install on <b>bare metal</b>, download an 
            <a href="ftp://mirror.checkdomain.de/dragonflybsd/iso-images/<?= $info->getHWInstallImage() ?>.bz2" 
			target="_blank">USB image</a> from a 
			<a href="https://www.dragonflybsd.org/mirrors/" target="_blank">DragonFlyBSD mirror</a>, 
			uncompress it (<code>bzip2 -d <?= $info->getHWInstallImage() ?>.bz2</code>) and burn it onto an USB stick<br />
			<?php require_once 'burn-usb.php'; ?></li>
			<li>To install a <b>virtual machine</b>, download an 
            <a href="ftp://mirror.checkdomain.de/dragonflybsd/iso-images/<?= $info->getVMInstallImage() ?>" 
			target="_blank">ISO image</a>, also from a 
			<a href="https://www.dragonflybsd.org/mirrors/" target="_blank">DragonFlyBSD mirror</a>, 
			and save it to your VM host's ISO folder.<br />You can also test a 
			<a href="ftp://mirror.checkdomain.de/dragonflybsd/snapshots/x86_64/images/" target="_blank">snapshot</a> 
			of the development version.</li>
			<li>Install DragonFlyBSD and reboot</li>
			<li>Log in as root</li>
		</ul>
	</body>
</html>
