<!DOCTYPE html>
<html>
	<head>
		<title>FreeBSD installation procedure</title>
		<meta charset="UTF-8" />
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Expires" content="0" />
		<meta http-equiv="Pragma" content="no-cache" />
		<style>
			ul > li {
				padding-bottom: 0.5em;
			}
		</style>
	</head>
	<body>
		<ul>
			<li>To install on <b>bare metal</b>, download a 
            <a href="http://www1.de.freebsd.org/freebsd/releases/ISO-IMAGES/<?= $info->getVersionNumber() ?>/<?= $info->getHWInstallImage() ?>" 
			target="_blank">memstick image</a> from a 
			<a href="https://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/mirrors-ftp.html" target="_blank">FreeBSD mirror</a> 
			and burn it onto an USB stick<br />
			<?php require_once 'burn-usb.php'; ?></li>
			<li>To install a <b>virtual machine</b>, download a 
            <a href="http://www1.de.freebsd.org/freebsd/releases/ISO-IMAGES/<?= $info->getVersionNumber() ?>/<?= $info->getVMInstallImage() ?>" 
			target="_blank">disc1 ISO</a>, also from a 
			<a href="https://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/mirrors-ftp.html" 
			target="_blank">FreeBSD mirror</a>, 
			and save it to your VM host's ISO folder.<br />You can also test a snapshot of 
			<a href="http://www1.de.freebsd.org/freebsd/snapshots/ISO-IMAGES/<?= $info->getVersionNumber() ?>/" target="_blank">STABLE</a> 
			or <a href="http://www1.de.freebsd.org/freebsd/snapshots/ISO-IMAGES/<?= intval($info->getVersionNumber()) + 1 ?>.0/" target="_blank">CURRENT</a>.</li>
			<li>Install FreeBSD and reboot</li>
			<li>Log in as root</li>
			<li>Run: <code>freebsd-update fetch install</code></li>
			<li>Reboot again</li>
			<li>Log in as root</li>
			<li>Run: <code>sh</code> <i>(root's default shell is unfortunately csh)</i></li>
		</ul>
	</body>
</html>
