<!DOCTYPE html>
<html>
	<head>
		<title>OpenBSD installation procedure</title>
		<meta charset="UTF-8" />
		<style>
			ul > li {
				padding-bottom: 0.5em;
			}
		</style>
	</head>
	<body>
		<ul>
			<li>To install on <b>bare metal</b>, download an 
            <a href="https://cdn.openbsd.org/pub/OpenBSD/<?= $info->getVersionNumber() ?>/amd64/<?= $info->getHWInstallImage() ?>" 
			target="_blank">USB image</a> from the 
			<a href="https://www.openbsd.org/faq/faq4.html#Download" target="_blank">OpenBSD download page</a>
			and burn it onto an USB stick<br />
			<?php require_once 'burn-usb.php'; ?></li>
			<li>To install a <b>virtual machine</b>, download a 
            <a href="https://cdn.openbsd.org/pub/OpenBSD/<?= $info->getVersionNumber() ?>/amd64/<?= $info->getVMInstallImage() ?>" 
			target="_blank">ISO image</a>, also from the
			<a href="https://www.openbsd.org/faq/faq4.html#Download" target="_blank">OpenBSD download page</a>
			and save it to your VM host's ISO folder.<br />You can also test a 
			<a href="https://cdn.openbsd.org/pub/OpenBSD/snapshots/amd64/" target="_blank">snapshot</a> 
			of the development version.</li>
			<li>Install OpenBSD and reboot</li>
			<li>Log in as root</li>
		</ul>
	</body>
</html>
