<!DOCTYPE html><?php
$currentImagesUrl = "http://nycdn.netbsd.org/pub/NetBSD-daily/HEAD/latest/images/";
$stableImagesUrl = "http://nycdn.netbsd.org/pub/NetBSD-daily/netbsd-" . $info->getVersionMajor() . "/latest/images/";
$releaseImagesUrl = "https://cdn.netbsd.org/pub/NetBSD/NetBSD-" . $info->getVersionNumber() . "/images/";
?>
<html>
	<head>
		<title>NetBSD installation procedure</title>
		<meta charset="UTF-8" />
		<style>
			ul > li {
				padding-bottom: 0.5em;
			}
		</style>
	</head>
	<body>
		<p>NetBSD comes in 3 branches:</p>
		<ul>
			<li><a href="<?= $currentImagesUrl ?>" target="_blank">CURRENT</a> 
			is where development takes place. You may want to use it 
			if you're involved in NetBSD development, or you have some 
			specific issue (e.g. hardware support) and want to check 
			if it will be solved in the upcoming major release, for 
			instance.</li>
			<li><a href="<?= $releaseImagesUrl ?>" target="_blank">RELEASE</a> 
			is at the other end of the spectrum. Its features are well 
			defined (documented in the release notes) and you can find 
			its dowmload link on the main page of the NetBSD website.</li>
			<li><a href="<?= $stableImagesUrl ?>" target="_blank">STABLE</a> 
			is the latest RELEASE version with the latest bug fixes 
			applied, including security patches. <b>This is probably 
			the branch you want to use.</b></li>
		</ul>
		<p>In case you've installed the RELEASE image, you can always 
		upgrade later to the STABLE branch without having to do a full 
		reinstallation, but just use <code>sysinst</code> to <a 
		href="https://www.unitedbsd.com/d/110-upgrading-netbsd-using-sysinst" 
		target="_blank">upgrade your system</a>.</p>
		<ul>
			<li>To install on <b>bare metal</b>, download a 
            <a href="<?= $stableImagesUrl . $info->getHWInstallImage() ?>.gz" 
			target="_blank">USB image</a> from the 
			<a href="https://www.netbsd.org/" target="_blank">NetBSD home page</a>, 
			uncompress it (<code>gzip -d <?= $info->getHWInstallImage() 
			?>.gz</code>) and burn it onto an USB stick<br />
			<?php require_once 'burn-usb.php'; ?></li>
			<li>To install a <b>virtual machine</b>, download an 
            <a href="<?= $stableImagesUrl . $info->getVMInstallImage() ?>" 
			target="_blank">ISO image</a>, also from the 
			<a href="https://www.netbsd.org/" target="_blank">NetBSD home page</a>
			and save it to your VM host's ISO folder.</li>
			<li>Install NetBSD and reboot</li>
			<li>Log in as root</li>
		</ul>
	</body>
</html>
