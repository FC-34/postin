<?php 
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
 */
?>
		<meta charset="UTF-8" />
		<style>
body, button, input, select {
	font-family: sans-serif;
	font-size: 12pt;
}

hr {
	width: 100%;
	height: 0;
	border-bottom: 0;
}

#top {
	display: flex;
	column-gap: 1em;
}

#title > h1 {
	margin-bottom: 0;
}

#title > p {
	font-style: italic;
}

.installerLogoWrapper, .mainLogoWrapper {
	margin: 0;
	align-self: flex-start;
}

.mainLogoFrame {
	--logo-size: 120px;
}

.installerLogoFrame {
	--logo-size: 150px;
}

.installerLogoFrame, .mainLogoFrame {
	text-align: center;
	display: table-cell;
	width: var(--logo-size);
	height: var(--logo-size);
	vertical-align: middle;
}

.installerLogoImg, .mainLogoImg {
	max-width: 100%;
	max-height: 100%;
	display: block;
	margin: 0 auto;
}

.installerLogoPath {
	display: none;
}
		</style>
