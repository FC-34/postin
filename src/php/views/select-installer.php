<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
 */
?>
<!DOCTYPE html>
<html>
	<head>
		<title>postin - Step 1: Select an installer</title>
<?php
	require 'common-head.php';
?>
		<style>
#selector {
	display: flex;
}

#selector .logoFrame {
	--logo-size: 150px;
	width: var(--logo-size);
	height: var(--logo-size);
	vertical-align: top;
}

#installers {
	margin-left: 1em;
	margin-right: 1em;
}

#info {
	max-width: 25em;
	align-self: flex-start;
}

#info > div > p:first-child {
	margin-top: 0;
}

#info > div > p {
	text-align: justify;
}

.logoPath {
	display: none;
	visibility: hidden;
}

.inactiveInfo {
	display: none;
}

.activeInfo {
	display: block;
}
		</style>
		<script>
function onClickInstaller(radioDiv) {
	let newRadioBtn = radioDiv.querySelector('input[type="radio"]');
	newRadioBtn.checked = true;
	let installerId = newRadioBtn.value;
	let installerInfoSelector = '#' + installerId + 'Info';
	
	let logoImg = document.querySelector('.installerLogoImg');
	
	if (logoImg) {
		logoImg.src = document.querySelector(installerInfoSelector + ' > .installerLogoPath').textContent;
		logoImg.alt = installerId + ' logo';
	}
	
	let previousDiv = document.querySelector('.activeInfo');
	
	if (previousDiv) {
		previousDiv.className = 'inactiveInfo';
	}
	
	let currentDiv = document.querySelector(installerInfoSelector);
	
	if (currentDiv) {
		currentDiv.className = 'activeInfo';
	}
}
		</script>
	</head>
	<body>
<?php
	require 'common-body.php';
?>
		<h2>Step 1: Select an installer</h2>
		<form action="" method="post">
			<div id="selector">
				<div class="installerLogoWrapper">
					<div class="installerLogoFrame">
					<?php
						$installerLogoPath = $appConfig->locateResource('php/images/' . $appConfig->installers[0]->getLogo()  . '.svg');
						
						if ($installerLogoPath == null) {
							$installerLogoPath = '';
						}
					?>
						<img class="installerLogoImg" src="<?= $installerLogoPath ?>" 
							alt="<?= $appConfig->installers[0]->getLogo() ?> logo" />
					</div>
				</div>
				<div id="installers">
				<?php
					$checked = 'checked';
					
					foreach ($appConfig->installers as $installer) {
				?>
					<div onclick="onClickInstaller(this)">
						<input type="radio" name="installer" value="<?= $installer->getId() ?>" <?= $checked ?> />
						<label><?= $installer->getDescription() ?></label>
					</div>
				<?php
						$checked = '';
					}
				?>
					<br />
				</div>
				<div id="info">
				<?php
					$className = 'active';
					
					foreach ($installerIntroduction as $installerId => $installerInfo) {
						$installerLogoPath = $appConfig->locateResource('php/images/' . $appConfig->getInstallerById($installerId)->getLogo()  . '.svg');
						
						if ($installerLogoPath == null) {
							$installerLogoPath = '';
						}
				?>
					<div id="<?= $installerId ?>Info" class="<?= $className ?>Info">
<?= $installerInfo ?>
						<span class="installerLogoPath"><?= $installerLogoPath ?></span>
					</div>
				<?php
						$className = 'inactive';
					}
				?>
				</div>
			</div>
			<hr />
			<div>
				<button type="submit">Next</button>
			</div>
		</form>
	</body>
</html>
