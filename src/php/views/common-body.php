<?php 
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
 */
	$mainLogoUrl = null;
	$title = 'postin - The post-installation assistant';
	$subtitle = 'Relax while <a href="https://gitlab.com/20-100-2fe/postin" target="_blank">postin</a> installs for you!';
	
	if (isset($appConfig)) {
		$mainLogoUrl = $appConfig->locateResource('php/images/' . $appConfig->logo);
		
		if ($appConfig->customTitle !== null) {
			$title = $appConfig->customTitle;
		}
		
		if ($appConfig->customSubTitle !== null) {
			$subtitle = $appConfig->customSubTitle;
		}
	}
?>
		<div id="top">
<?php
				if ($mainLogoUrl !== null) {
?>
			<div class="mainLogoWrapper">
				<div class="mainLogoFrame">
					<img class="mainLogoImg" src="<?= $mainLogoUrl ?>" 
						alt="Logo (<?= $appConfig->logo ?>)" />
				</div>
			</div>
<?php
				}
?>
			<div id="title">
				<h1><?= $title ?></h1>
<?php
				if (strlen($subtitle) > 0) {
?>
				<p><?= $subtitle ?></p>
<?php
				}
?>
			</div>
		</div>
		<hr />
