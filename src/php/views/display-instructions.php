<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
 */
	$pageUrl = $appConfig->getBaseUrl();
	$httpClient = $selectedInstaller->info->getHttpClientCommand();
	$stepName = '';
	$backInstaller = '';
	
	if ($hasStep2) {
		$stepNum = 2;
		
		if ($hasStep1) {
			$stepNum++;
		}
		
		$stepName = 'Step ' . $stepNum . ': ';
		$backInstaller = $selectedInstaller->getId();
	}
	
	$scriptUrl = $pageUrl . '?installer=' . $selectedInstaller->getId() . '&amp;features=' . $featureList;
?>
<!DOCTYPE html>
<html>
	<head>
		<title>postin - <?= $stepName ?>Proceed with installation</title>
<?php
	require 'common-head.php';
?>
		<style>
ul > li {
	padding-bottom: 0.5em;
}
		</style>
	</head>
	<body>
<?php
	require 'common-body.php';
?>
		<h2><?= $stepName ?>Proceed with installation</h2>
		<div>
<?php
	$info = $selectedInstaller->info;
	require $installationInstructions;
?>
			<ul>
				<li>Execute the following command:<br />
				<pre><?= $httpClient ?> - '<?= $scriptUrl ?>' | sh 2&gt; error.log</pre>
				Please note the single quotes around the URL are <b>required</b>!<br />
				Also note standard error is redirected to the 'error.log' file to facilitate troubleshooting.<br />
				<a href="<?= $scriptUrl ?>" target="_blank">Click here</a> if you want to see the generated script.</li>
				<li>Reboot and enjoy!</li>
			</ul>
		</div>
		<hr />
		<div>
			<form action="" method="post">
				<input type="hidden" name="installer" value="<?= $backInstaller ?>" />
				<button type="submit">Back</button>
			</form>
		</div>
	</body>
</html>
