<?php 
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
 */
	$stepNum = 1;
	
	if ($hasStep1) {
		$stepNum++;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>postin - Step <?= $stepNum ?>: Select desired features</title>
<?php
	require 'common-head.php';
?>
		<script>
function onClickRadioDiv(ev, radioDiv) {
	let newRadioBtn = radioDiv.querySelector('input[type="radio"]');
	newRadioBtn.checked = true;
	let divClass = null;
	
	if (newRadioBtn.className === 'desktop') {
		divClass = 'visible';
	} else if (newRadioBtn.className === 'console') {
		divClass = 'hidden';
	}
	
	if (divClass) {
		document.getElementById('desktopTitle').className = divClass;
		document.getElementById('desktopDiv').className = divClass;
	}
}

function onClickCheckBoxDiv(ev, radioDiv) {
	if (ev.target.localName !== 'input') {
		let newRadioBtn = radioDiv.querySelector('input[type="checkbox"]');
		newRadioBtn.checked = !newRadioBtn.checked;
	}
}
		</script>
		<style>
.visible {
}

.hidden {
	display: none;
	visibility: hidden;
}
		</style>
	</head>
	<body>
<?php
	require 'common-body.php';
?>
		<h2>Step <?= $stepNum ?>: Select desired features</h2>
		<div>
			<form id="selectorForm" action="" method="post">
				<input type="hidden" name="installer" value="<?= $selectedInstaller->getId() ?>" />
				<h3>Target use case</h3>
			<?php
				foreach ($selectedInstaller->profiles as $profile) {
					$radioClass = in_array(Feature::DESKTOP, $profile->features) ? 'desktop' : 'console';
			?>
				<div onclick="onClickRadioDiv(event, this)">
					<input type="radio" name="profile" value="<?= $profile->id ?>" class="<?= $radioClass ?>" />
					<label><?= $profile->description ?></label>
				</div>
			<?php
				}
			?>
				<h3 id="desktopTitle" class="hidden">Desktop preferences</h3>
				<div id="desktopDiv" class="hidden" style="display: flex;">
					<div>
			<?php
				$supportedDE = $selectedInstaller->info->getDesktopEnvironments();
				$selectedDE = count($supportedDE) > 0 ? $supportedDE[0] : 'none';
				
				if ($selectedDE !== 'none') {
			?>
						Desktop Environment <select name="de">
			<?php
					foreach ($supportedDE as $de) {
			?>
							<option value="<?= $de ?>"<?= $de === $selectedDE ? ' selected' : '' ?>><?= Feature::getName($de) ?></option>
			<?php
					}
			?>
						</select>
						&nbsp;&nbsp;
			<?php
				}
			?>
					</div>
					<div>
			<?php
				$supportedDM = $selectedInstaller->info->getDisplayManagers();
				$selectedDM = count($supportedDM) > 0 ? $supportedDM[0] : 'none';
			?>
						Display Manager <select name="dm">
							<option value="none"<?= 'none' === $selectedDM ? ' selected' : '' ?>>None</option>
			<?php
					foreach ($supportedDM as $dm) {
			?>
							<option value="<?= $dm ?>"<?= $dm === $selectedDM ? ' selected' : '' ?>><?= Feature::getName($dm) ?></option>
			<?php
					}
			?>
						</select>
						&nbsp;&nbsp;
					</div>
				</div>
			<?php
				$selectedCountry = '';
				
				switch (count($appConfig->countries)) {
				case 1:
					$selectedCountry = $appConfig->countries[0]->id;
				case 0:
			?>
				<input type="hidden" name="country" value="<?= $selectedCountry ?>" />
			<?php
					break;
				
				default:
			?>
				<h3>Country</h3>
				<div>
					Country <select name="country">
			<?php
					$selectedCountry = $appConfig->defaultCountry !== null ? $appConfig->defaultCountry : $appConfig->countries[0]->id;
					
					foreach ($appConfig->countries as $country) {
			?>
						<option value="<?= $country->id ?>"<?= $country->id === $selectedCountry ? ' selected' : '' ?>><?= $country->description ?></option>
			<?php
					}
			?>
					</select>
				</div>
			<?php
					break;
				}
				
				if (count($selectedInstaller->features) > 0) {
			?>
				<br />
				<h3>Optional features</h3>
			<?php
					foreach ($selectedInstaller->features as $feature) {
			?>
				<div onclick="onClickCheckBoxDiv(event, this)">
					<input type="checkbox" name="<?= $feature->id ?>" value="feature"<?= $selectedInstaller->isPreselected($feature->id) ? ' checked' : '' ?> />
					<label><?= $feature->description ?></label>
				</div>
			<?php
					}
				}
			?>
			</form>
		</div>
		<hr />
		<div>
			<?php
				if ($hasStep1) {
			?>
			<form id="backForm" action="" method="get">
			</form>
			<button onclick="document.getElementById('backForm').submit();">Back</button>
			<?php
				}
			?>
			<button onclick="document.getElementById('selectorForm').submit();">Next</button>
		</div>
	</body>
</html>
