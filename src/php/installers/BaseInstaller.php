<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'constants/Defaults.php';
require_once 'constants/Feature.php';
require_once 'constants/Location.php';
require_once 'choices/FeatureChoice.php';
require_once 'choices/ProfileChoice.php';

abstract class BaseInstaller {
	public function __construct() {
		$this->profiles = array();
		array_push($this->profiles,	new ProfileChoice('', 'none', 'Add features to an existing installation', []));
		$this->recipes = array();
		$this->features = array();
		$this->debugMode = FALSE;
		$this->nestingLevel = 0;
		$this->preselectedFeatures = [ Feature::LATEST ];
	}
	
	protected $debugMode;
	public $info;
	public $profiles;
	public $features;
	public $preselectedFeatures;
	private $mergedFeatures;
	
	public abstract function getApplicationsPrefix();
	public abstract function getAppIconsDir();
	public abstract function getAppLaunchersDir();
	public abstract function configureInstaller();
	public abstract function configureInitialInstallation();
	public abstract function configureFeature($featureId);
	public abstract function installFeature($featureId);
	public abstract function completeFeature($featureId);
	public abstract function completeInstallation();
	
	public function setDebugMode($debugMode) {
		$this->debugMode = $debugMode;
		
		return $this;
	}
	
	public function setPreselectedFeatures($preselectedFeatures) {
		$this->preselectedFeatures = $preselectedFeatures;
		
		return $this;
	}
	
	public function isPreselected($featureId) {
		return $this->preselectedFeatures !== null 
			&& $featureId !== null
			&& in_array($featureId, $this->preselectedFeatures);
	}
	
	public function isSelected($featureId) {
		$rc = FALSE;
		
		if (is_array($featureId)) {
			foreach ($featureId as $id) {
				$rc = $this->isSelected($id);
				
				if (!$rc) {
					break;
				}
			}
		} else {
			$rc = $this->mergedFeatures !== null 
				&& $featureId !== null
				&& in_array($featureId, $this->mergedFeatures);
		}
		
		return $rc;
	}
	
	protected function mergeProfiles($descendantDefs) {
		$parentDefs = $this->profiles;
		$this->profiles = array();
		
		if ($parentDefs !== null) {
			foreach ($parentDefs as $parentDef) {
				array_push($this->profiles, $parentDef);
			}
		}
		
		foreach ($descendantDefs as $currentDef) {
			array_push($this->profiles, $currentDef);
		}
	}
	
	public static function mergeStringArrays($parentDefs, $descendantDefs) {
		$result = array();
		
		if ($parentDefs !== null) {
			foreach ($parentDefs as $parentDef) {
				array_push($result, $parentDef);
			}
		}
		
		foreach ($descendantDefs as $currentDef) {
			if (!in_array($currentDef, $result)) {
				array_push($result, $currentDef);
			}
		}
		
		return $result;
	}
	
	protected function mergeFeatures($descendantDefs) {
		$this->features = self::mergeStringArrays($this->features, $descendantDefs);
	}
	
	public function getId() {
		return $this->info->getId();
	}
	
	public function getDescription() {
		$result = $this->info->getSystemName();
		
		if ($this->info->getVersionNumber() !== null) {
			$result = $result . ' ' . $this->info->getVersionNumber();
		}
		
		if ($this->info->getVersionName() !== null) {
			$result = $result . ' ' . $this->info->getVersionName();
		}
		
		return $result;
	}
	
	public function getLogo() {
		return $this->getId();
	}
	
	public function getProfileById($profileId) {
		foreach ($this->profiles as $profile) {
			if ($profile->id === $profileId) {
				return $profile;
			}
		}
		
		return null;
	}
	
	protected function generateEnvironment() {
	}
	
	private function getJavaPrefix($appConfig) {
		$result = $appConfig->javaPrefix;
		
		if ($result === null || strlen($result) === 0) {
			$result = $this->info->getInstallationPrefix();
		}
		
		return $result;
	}
	
	private function substituteVariables($val, $appConfig) {
		$result = $val;
		$result = str_replace('INSTALL_PREFIX', $this->info->getInstallationPrefix(), $result);
		$result = str_replace('CONFIG_PREFIX', $this->info->getConfigurationPrefix(), $result);
		$result = str_replace('JAVA_PREFIX', $this->getJavaPrefix($appConfig), $result);
		$result = str_replace('APPS_PREFIX', $this->getApplicationsPrefix(), $result);
		$result = str_replace('APP_ICONS_DIR', $this->getAppIconsDir(), $result);
		$result = str_replace('APP_LAUNCHERS_DIR', $this->getAppLaunchersDir(), $result);
		
		return $result;
	}
	
	public function generateScript($appConfig, $requestedFeatures) {
		$this->mergedFeatures = array();
		
		if ($this->preselectedFeatures !== null) {
			foreach ($this->preselectedFeatures as $featureId) {
				if (!in_array($featureId, $this->mergedFeatures)) {
					array_push($this->mergedFeatures, $featureId);
				}
			}
		}
		
		if ($requestedFeatures !== null) {
			foreach ($requestedFeatures as $featureId) {
				if (!in_array($featureId, $this->mergedFeatures)) {
					array_push($this->mergedFeatures, $featureId);
				}
			}
		}
		
		if (in_array(Feature::VBOX_GUEST, $this->mergedFeatures)
			|| in_array(Feature::QEMU_GUEST, $this->mergedFeatures)
			|| in_array(Feature::VMWARE_GUEST, $this->mergedFeatures)) {
			if (!in_array(Feature::VM_INSTALL, $this->mergedFeatures)) {
				array_push($this->mergedFeatures, Feature::VM_INSTALL);
			}
		}
		
		$useLatestPackages = in_array(Feature::LATEST, $this->mergedFeatures);
		$initialInstall = in_array(Feature::CONSOLE, $this->mergedFeatures);

		if (!$this->debugMode) {
			print('USE_LATEST=' . ($useLatestPackages ? 'YES' : 'NO') . "\n");
			print('INITIAL_INSTALL=' . ($initialInstall ? 'YES' : 'NO') . "\n");
			print('FILES_DIR=' . Defaults::FILES_DIR . "\n");
			print('SCRIPTS_SUFFIX=' . Defaults::SCRIPTS_SUFFIX . "\n");
			print('PACKAGES_SUFFIX=' . Defaults::PACKAGES_SUFFIX . "\n");
			print('HTTP_CLIENT="' . $this->info->getHttpClientCommand() . "\"\n");
			print('SED_CMD="' . $this->info->getSedInlineCommand() . "\"\n");
			print('SERVER_ADDR=' . $appConfig->getServerAddress() . "\n");
			print('LOCAL_MIRROR=' . ($appConfig->isServerAddressInternal() ? 'http://mirror.local' : '') . "\n");
			
			print('INSTALL_PREFIX=' . $this->info->getInstallationPrefix() . "\n");
			print('CONFIG_PREFIX=' . $this->info->getConfigurationPrefix() . "\n");
			print('JAVA_PREFIX=' . $this->getJavaPrefix($appConfig) . "\n");
			print('APPS_PREFIX=' . $this->getApplicationsPrefix() . "\n");
			print('APP_ICONS_DIR=' . $this->getAppIconsDir() . "\n");
			print('APP_LAUNCHERS_DIR=' . $this->getAppLaunchersDir() . "\n");
			print('LOGIN_BACKGROUND=' . $this->substituteVariables($appConfig->loginBackground, $appConfig) . "\n");
			print('DESKTOP_BACKGROUND=' . $this->substituteVariables($appConfig->desktopBackground, $appConfig) . "\n");
			
			$this->generateEnvironment();
		}
		
		// The 'init' phase configures the machine so features can be installed
		// (implement helper commands, define repositories, update system, etc).
		// A working network connection is required, e.g. to download 
		// repository keys or update the system.

		// Installer initialization.
		$this->configureInstaller();

		if ($initialInstall) {
			// 'console' is included in all usage profiles but 'none', so we 
			// can reliably use it to detect an initial installation.
			// Initial installation initializations are only needed in case of an 
			// (e.g. repository, file system and user configuration).
			$this->configureInitialInstallation();
		}

		// Features initialization.
		foreach ($this->mergedFeatures as $featureId) {
			$this->configureFeature($featureId);
		}

		// The 'install' phase installs requested features.
		// A working network connection is obviously required.
		foreach ($this->mergedFeatures as $featureId) {
			$this->installFeature($featureId);
		}

		// The 'complete' phase performs feature post-installation configuration
		// and cleanup. A working network connection is NOT required and 
		// scriptlets should NOT assume there is one.
		foreach ($this->mergedFeatures as $featureId) {
			$this->completeFeature($featureId);
		}

		// Installer cleanup, if needed.
		$this->completeInstallation();
	}
	
	public function installPackages($packageNames) {
		print("echo '=== Installing packages: " . $packageNames . "' >&2\n");
		
		if (!$this->debugMode) {
			print("installPackages " . $packageNames . "\n");
		}
	}
	
	public function removePackages($packageNames) {
		print("echo '=== Removing packages: " . $packageNames . "' >&2\n");
		
		if (!$this->debugMode) {
			print("removePackages " . $packageNames . "\n");
		}
	}
	
	private function getResourceUrl($location, $sourceFile, $fileSuffix = null) {
		$resourceUrl = $sourceFile;
		
		if ($location === Location::LOCAL_FOLDER) {
			$resourcePath = Defaults::FILES_DIR . '/' . $sourceFile;
			$suffix = '';
			
			if ($fileSuffix !== null && strlen($fileSuffix) > 0) {
				$suffix = $fileSuffix;
				
				if (strpos($suffix, '.') !== 0) {
					$suffix = '.' . $suffix;
				}
			}
			
			$resourceUrl = AppConfig::getInstance()->locateResource($resourcePath . $suffix);
			
			if ($fileSuffix !== null && strlen($fileSuffix) > 0) {
				$resourceUrl = substr($resourceUrl, 0, strlen($resourceUrl) - strlen($suffix));
			}
		}
		
		return $resourceUrl;
	}
	
	public function installFile($location, $sourceFile, $targetDir, $targetFile = '') {
		print("echo '=== Installing file: " . $sourceFile . ' to: '  . $targetDir . "' >&2\n");
		
		if (!$this->debugMode) {
			$cmd = "installFile '" . $location . "' \"" . $this->getResourceUrl($location, $sourceFile) . '" "' . $targetDir . '"';
			
			if ($targetFile !== null && strlen($targetFile) > 0) {
				$cmd = $cmd . ' "' . $targetFile . '"';
			}
			
			print($cmd . "\n");
		}
	}
	
	public function installConfig($location, $sourceFile, $targetDir, $targetFile = '') {
		print("echo '=== Installing config. file: " . $sourceFile . ' to: '  . $targetDir . "' >&2\n");
		
		if (!$this->debugMode) {
			$cmd = "installConfig '" . $location . "' \"" . $this->getResourceUrl($location, $sourceFile, Defaults::CONFIG_SUFFIX) . '" "' . $targetDir . '"';
			
			if ($targetFile !== null && strlen($targetFile) > 0) {
				$cmd = $cmd . ' "' . $targetFile . '"';
			}
			
			print($cmd . "\n");
		}
	}
	
	public function installLauncherIfMissing($application, $location, $sourceFile) {
		print("echo '=== Installing application launcher: " . $sourceFile . ' application: '  . $application . "' >&2\n");
		
		if (!$this->debugMode) {
			$cmd = "installLauncherIfMissing '" . $application . "' '" . $location . "' \"" . $this->getResourceUrl($location, $sourceFile, Defaults::CONFIG_SUFFIX) . '"';
			print($cmd . "\n");
		}
	}
	
	public function installArchive($location, $sourceFile, $targetDir, $archiveSuffix = '') {
		print("echo '=== Installing archive: " . $sourceFile . ' to: '  . $targetDir . "' >&2\n");
		
		if (!$this->debugMode) {
			$suffix = ($archiveSuffix !== null && strlen($archiveSuffix) > 0) ? $archiveSuffix : Defaults::PACKAGES_SUFFIX;
			$cmd = "installArchive '" . $location . "' \"" . $this->getResourceUrl($location, $sourceFile, $suffix) . '" "' . $targetDir . '"';
			
			if ($archiveSuffix !== null && strlen($archiveSuffix) > 0) {
				$cmd = $cmd . ' "' . $archiveSuffix . '"';
			}
			
			print($cmd . "\n");
		}
	}
	
	public function executeScriptlet($scriptletPath) {
		$filePath = AppConfig::getInstance()->locateFile('sh/' . $scriptletPath . '.txt');
	   
		if ($filePath != null) {
			print("echo '=== Executing scriptlet: " . $scriptletPath . "' >&2\n");
		
			if (!$this->debugMode) {
				print(file_get_contents($filePath));
			}
		}
	}
	
	public function executeCommand($commandLine) {
		print("echo '=== Executing command: " . $commandLine . "' >&2\n");
		
		if (!$this->debugMode) {
			print($commandLine . "\n");
		}
	}
}
