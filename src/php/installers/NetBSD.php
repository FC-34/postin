<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'BaseBSDInstaller.php';
require_once 'NetBSDInfo.php';

class NetBSD extends BaseBSDInstaller {
	public function __construct() {
		parent::__construct();
		$this->info = new NetBSDInfo();
		$this->mergeFeatures([
			new FeatureChoice(NetBSDInfo::ID, Feature::LATEST, 'Use most recent packages ("current" instead of "quaterly")'),
			new FeatureChoice(NetBSDInfo::ID, 'modular-xorg', 'Use modular-xorg package instead of X11 sets from base'),
			new FeatureChoice(NetBSDInfo::ID, Feature::VMWARE_GUEST, 'Run as VMWare guest'),
			new FeatureChoice(NetBSDInfo::ID, Feature::QEMU_GUEST, 'Run as QEMU guest'),
		]);
	}

	public function configureInstaller() {
		parent::configureInstaller();
		
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInstaller()'\n");
		}

		$this->executeScriptlet(NetBSDInfo::ID . '/00-required');
		$this->executeScriptlet(NetBSDInfo::ID . '/01-specific');
		$this->executeScriptlet(NetBSDInfo::ID . '/02-packages');
	}

	public function configureInitialInstallation() {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInitialInstallation()'\n");
		}

		$this->executeScriptlet(NetBSDInfo::ID . '/10-initial-install');
	}

	public function installFeature($featureId) {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::installFeature($featureId)'\n");
		}

		$propagate = TRUE;

		switch ($featureId) {
		case Feature::CONSOLE:
			$this->installPackages('pkg_alternatives');
			$this->installPackages('p7zip vim rsync py38-pip ca-certificates pwgen');
			$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/shell/profile', '/etc');
			$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/shell/shrc', '/etc');
			$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/shell/98_local.sh', '/etc/profile.d');
			$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/shell/dot-profile', '/etc/skel', '.profile');
			$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/shell/dot-shrc', '/etc/skel', '.shrc');

			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/wifi/rc.wifi.conf', '${INSTALL_PREFIX}/share/examples/wifi');
				$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/wifi/ifconfig.iwm0', '${INSTALL_PREFIX}/share/examples/wifi');
				$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/wifi/wpa_supplicant.networks', '${INSTALL_PREFIX}/share/examples/wifi');
				
				$this->installPackages('fuse-exfat fuse-ntfs-3g');
				//$this->installPackages('ipmitool smartmontools');
			}
			break;

		case Feature::DESKTOP:
			// Only if they're missing
			$this->executeScriptlet(NetBSDInfo::ID . '/install-x11-sets');

			if ($this->isSelected('modular-xorg')) {
				$this->installPackages('modular-xorg');
				$this->executeScriptlet(NetBSDInfo::ID . '/modular-xorg');
			}

			if ($this->isSelected(Feature::MATE)) {
				$this->installPackages('mate');
				$this->installPackages('geany seahorse dconf-editor galculator');
				$this->installPackages('gst-plugins1-good gst-plugins1-bad gst-plugins1-ugly');
				$this->installPackages('numix-gtk-theme numix-icon-theme');
				
				if (!$this->isSelected(Feature::VM_INSTALL)) {
					$this->installPackages('transmission-gtk hexchat liferea');
				}
			}

			if ($this->isSelected(Feature::LXQT)) {
				$this->installPackages('lxqt lxqt-archiver');
				$this->installPackages('fbreader xpdf featherpad');
				$this->installPackages('numix-gtk-theme numix-icon-theme');
				// Missing: kColorPicker kadu
				
				if (!$this->isSelected(Feature::VM_INSTALL)) {
					$this->installPackages('quassel qbittorrent cutecom quiterss xscreensaver');
				}
			}

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/10-keyboard_ch-fr.conf', '/etc/X11/xorg.conf.d');
			}

			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installPackages('sane-backends');
				$this->installPackages('cups gutenprint-lib');
				$this->installPackages('qemu');
				//$this->installPackages('lvm2 hfsutils xfsprogs ntfsprogs dosfstools udfclient');
			}

			$this->installPackages('gettext-lib hal gamin');
			$this->installPackages('sound-theme-freedesktop xdg-user-dirs');
			break;

		case Feature::DISPLAY_MANAGER:
			if ($this->isSelected(Feature::SDDM)) {
				$this->installPackages('sddm');
			} else if ($this->isSelected(Feature::XDM)) {
				$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/xdm/Xresources', '/local/etc/X11/xdm');
				$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/xdm/Xsession', '/local/etc/X11/xdm');
				$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/xdm/Xsetup_0', '/local/etc/X11/xdm');
				$this->installFile(Location::LOCAL_FOLDER, 'netbsd/xdm/NetBSD-xdm.xpm', '/local/etc/X11/xdm');
				$this->executeScriptlet(NetBSDInfo::ID . '/xdm');
				$propagate = FALSE;
			}
			break;

		case Feature::VNC:
			$this->installPackages('tigervnc');
			break;

		case Feature::SAMBA:
			$this->installPackages('samba4');
			break;

		case Feature::OFFICE_SUITE:
			$this->installPackages('libreoffice');
			$this->installPackages('gimp');
			$this->installPackages('inkscape');
			$this->installPackages('hyphen hunspell-en_US');

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installPackages('hunspell-de hunspell-en_GB hunspell-fr_FR');
			}
			break;

		case Feature::FIREFOX:
			switch ($this->getFirefoxVersion()) {
			case Feature::FIREFOX_ESR:
				$this->installPackages('firefox-esr');
				break;

			case Feature::FIREFOX_LATEST:
				$this->installPackages('firefox');
				break;
			}
			break;

		case Feature::THUNDERBIRD:
			$this->installPackages('thunderbird');
			break;

		case Feature::IM_CLIENT:
			$this->installPackages('pidgin pidgin-libnotify pidgin-otr skypeweb-purple');
			break;

		case Feature::POSTGRESQL:
			$this->installPackages('postgresql12-server');

			if ($this->isSelected(Feature::DEVELOP)) {
				$this->installPackages('pgadmin3 postgresql12-docs postgresql12-contrib');
			}
			break;

		case Feature::OPENJDK:
			$this->installPackages('openjdk8');
			$this->installPackages('openjdk11');
			break;

		case Feature::NODEJS:
			$this->installPackages('nodejs npm');
			break;

		case Feature::SCM:
			if (!$this->isSelected(Feature::VM_INSTALL)) {
				// NOTE subversion installs apache24 :(
				$this->installPackages('cvs mercurial');
			}

			$this->installPackages('git');
			break;

		case Feature::TCL_TK:
			$this->installPackages('tcl86 tk86');
			break;

		case Feature::CMAKE:
			$this->installPackages('cmake cmake-gui meson');
			$this->installFile(Location::LOCAL_FOLDER, 'icons/cmake.svg', '${APP_ICONS_DIR}', 'CMakeSetup.svg');
			break;

		case Feature::DEVELOP:
			$this->installPackages('pkgconf pkglint cwrappers digest gdb zeal');
			$this->installPackages('clang clang-tools-extra lldb');
			$this->installPackages('codeblocks cscope cppcheck valgrind');
			break;

		case Feature::APACHE_PHP:
			$this->installPackages('apache ap24-php74 php74-pdo_pqsql php74-json php74-iconv php74-gd php74-exif php74-curl php74-tidy');
			$this->installPackages('php74-zip php74-zlib php74-bz2 php74-xmlrpc php74-gettext php74-mbstring php74-intl php74-sodium');
			$this->installPackages('php74-xsl php74-opcache php74-soap php74-enchant php74-ftp php74-bcmath php74-calendar');
			// $this->installPackages('php74-posix php74-shmop php74-sysvmsg php74-sysvsem php74-sysvshm php74-sockets');
			// $this->installPackages('php74-pdo_mysql php74-pdo_sqlite php74-pdo_dblib php74-pgsql php74-sqlite3');
			// $this->installPackages('php74-ldap php74-dba php74-snmp php74-imap php74-pspell php74-pcntl php74-gmp');
			// Also, php74-fpm
			break;

		case Feature::VMWARE_GUEST:
			$this->installPackages('open-vm-tools');
			break;

		case Feature::QEMU_GUEST:
			# qemu-ga (= GUEST agent) is part of the qemu (hypervisor HOST) package :(
			$this->installPackages('qemu');
			break;

		case Feature::CALIBRE:
			// Only version 3 is available
			break;

		case Feature::ECLIPSE:
			// Only version 3.0 is available
			break;

		case Feature::VSCODE:
			// NOT available at all
			break;

		case Feature::NUMIX:
			parent::installFeature($featureId);
			$propagate = FALSE;
			$this->installFile(Location::LOCAL_FOLDER, 'netbsd/ctwm/menu.xbm', '${INSTALL_PREFIX}/share/themes/Numix/openbox-3');
			$this->installConfig(Location::LOCAL_FOLDER, 'netbsd/ctwm/ctwmrc', '${INSTALL_PREFIX}/share/themes/Numix/ctwm');
			// To use the Numix CTWM theme:
			//
			// echo "exec ctwm -W" > ${HOME}/.xinitrc
			// cp /usr/pkg/share/themes/Numix/ctwm/ctwmrc ${HOME}/.ctwmrc
			// # and customise menus to taste
			break;
		}

		if ($propagate) {
			parent::installFeature($featureId);
		}
	}

	public function completeFeature($featureId) {
		parent::completeFeature($featureId);

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::completeFeature($featureId)'\n");
		}

		switch ($featureId) {
		case Feature::CONSOLE:
			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->executeScriptlet(NetBSDInfo::ID . '/luxembourg');
			}

			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->executeCommand('enableService devpubd');
			}
			break;

		case Feature::DESKTOP:
			$this->executeScriptlet(NetBSDInfo::ID . '/desktop-complete');
			break;
		}
	}
}
