<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'BaseLinuxInfo.php';

class DebianInfo extends BaseLinuxInfo {
	const ID = 'debian';
	
	public function getId() {
		return self::ID;
	}
	
	public function getSystemName() {
		return 'Debian';
	}
	
	public function getVersionNumber() {
		return '10.8';
	}
	
	public function getVersionName() {
		return 'buster';
	}
	
	public function getBaseCodeName() {
		// Any version of Debian is based on itself.
		return '$(lsb_release -s -c)';
	}
	
	public function getDistServers() {
		return 'http://deb.debian.org/debian http://security.debian.org/debian-security';
	}
	
	public function getHWInstallImage() {
		return 'firmware-' . $this->getVersionNumber() . '.0-amd64-netinst.iso';
	}
	
	public function getVMInstallImage() {
		return 'debian-' . $this->getVersionNumber() . '.0-amd64-netinst.iso';
	}

}
