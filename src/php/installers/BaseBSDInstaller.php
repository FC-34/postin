<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'BaseUnixInstaller.php';
require_once 'BaseBSDInfo.php';

abstract class BaseBSDInstaller extends BaseUnixInstaller {
	protected function generateEnvironment() {
		parent::generateEnvironment();
		print('RC_CONF=' . $this->info->getSystemConfigurationFile() . "\n");
	}
	
	public function configureInstaller() {
		parent::configureInstaller();
		
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInstaller()'\n");
		}
		
		$this->executeScriptlet(BaseBSDInfo::ID . '/00-psysrc');
		$this->executeScriptlet(BaseBSDInfo::ID . '/01-specific');
	}
	
	public function installFeature($featureId) {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::installFeature($featureId)'\n");
		}
		
		$propagate = TRUE;
		
		switch ($featureId) {
		case Feature::DEVELOP:
			$this->installPackages('gmake autoconf autoconf-archive automake libtool gettext');
			$this->executeScriptlet(BaseBSDInfo::ID . '/develop');
			break;
		}
		
		if ($propagate) {
			parent::installFeature($featureId);
		}
	}
}
