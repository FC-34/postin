<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'BaseLinuxInstaller.php';
require_once 'DebianInfo.php';

class Debian extends BaseLinuxInstaller {
	public function __construct() {
		parent::__construct();
		$this->info = new DebianInfo();
		$this->mergeDebianFeatures();
	}
	
	protected function mergeDebianFeatures() {
		$this->mergeFeatures([
			new FeatureChoice(DebianInfo::ID, Feature::LATEST, 'Use most recent packages ("backports" instead of default)'),
			new FeatureChoice(DebianInfo::ID, Feature::VBOX_HOST, 'Install VirtualBox host'),
			new FeatureChoice(DebianInfo::ID, Feature::VMWARE_GUEST, 'Run as VMWare guest'),
			new FeatureChoice(DebianInfo::ID, Feature::QEMU_GUEST, 'Run as QEMU guest'),
		]);
	}
	
	protected function generateEnvironment() {
		parent::generateEnvironment();
		print('BASED_ON_DIST=' . $this->info->getBaseCodeName() . "\n");
		print('DIST_SERVERS="' . $this->info->getDistServers() . "\"\n");
	}

	public function configureInstaller() {
		parent::configureInstaller();

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInstaller()'\n");
		}

		$this->executeScriptlet(DebianInfo::ID . '/01-specific');
	}

	public function configureInitialInstallation() {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInitialInstallation()'\n");
		}

		$this->executeScriptlet(DebianInfo::ID . '/10-initial-install');
		// These MUST be installed BEFORE adding new repositories,
		// i.e. before calling configureFeature().
		$this->installPackages('software-properties-common apt-transport-https gnupg2');
	}

	public function configureFeature($featureId) {
		parent::configureFeature($featureId);

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureFeature($featureId)'\n");
		}

		switch ($featureId) {
		case Feature::FIREFOX:
			if ($this->getFirefoxVersion() === Feature::FIREFOX_LATEST) {
				$this->executeScriptlet('linux/mozilla-init');
			}
			break;

		case Feature::OPENJDK:
			$this->executeScriptlet(DebianInfo::ID . '/adoptopenjdk-init');
			break;

		case Feature::POSTGRESQL:
			$this->executeScriptlet(DebianInfo::ID . '/postgresql-init');
			break;

		case Feature::NODEJS:
			$this->executeScriptlet(DebianInfo::ID . '/nodejs-init');
			break;

		case Feature::ECLIPSE:
			$this->executeScriptlet('linux/eclipse-init');
			break;

		case Feature::VSCODE:
			$this->executeScriptlet(DebianInfo::ID . '/vscode-init');
			break;

		case Feature::VBOX_HOST:
		case 'vbox-extpack':
			$this->executeScriptlet(DebianInfo::ID . '/virtualbox-init');
			break;
		}
	}

	public function installFeature($featureId) {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::installFeature($featureId)'\n");
		}

		$propagate = TRUE;

		switch ($featureId) {
		case Feature::CONSOLE:
			// NOTE: Package 'whois' brings in /usr/bin/mkpasswd
			$this->installPackages('ufw rhash rsync chrony neovim pwgen whois inetutils-ftp python3-pip python3-dev');
			$this->installPackages('net-tools sudo');
			//$this->installPackages('ipmitool smartmontools');
			break;

		case Feature::DESKTOP:
			$this->installPackages('xorg');

			if ($this->isSelected(Feature::MATE)) {
				$this->installPackages('mate-desktop-environment');
				$this->installPackages('caja-open-terminal caja-sendto geany geany-plugins');
				$this->installPackages('seahorse galculator gjs orca');
				$this->installPackages('numix-gtk-theme numix-icon-theme');

				if (!$this->isSelected(Feature::VM_INSTALL)) {
					$this->installPackages('network-manager-gnome');
					$this->installPackages('transmission-gtk blueman gscan2pdf gufw');
					$this->installPackages('system-config-printer gnome-disk-utility dconf-editor gparted');
					$this->installPackages('hexchat hexchat-otr hexchat-plugins liferea');
				}
			}

			if ($this->isSelected(Feature::LXQT)) {
				$this->installPackages('lxqt-core lxqt-themes lximage-qt xqt-about lxqt-powermanagement');
				$this->installPackages('lxqt-sudo pavucontrol-qt qterminal lxqt-openssh-askpass obconf-qt');
				$this->installPackages('fbreader xpdf featherpad screengrab');
				$this->installPackages('numix-gtk-theme numix-icon-theme');
				// Missing: lxqt-archiver kColorPicker
				
				if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installPackages('quassel qbittorrent cutecom kadu quiterss');
					$this->installPackages('nm-tray xscreensaver gvfs blueman');
				}
			}

			if ($this->isSelected(Feature::KDE)) {
				$this->installPackages('kde-runtime xdg-desktop-portal xdg-desktop-portal-kde');
				$this->installPackages('kde-plasma-desktop plasma-workspace-wayland kdialog keditbookmarks kfind konsole');
				$this->installPackages('plasma-runners-addons plasma-wallpapers-addons plasma-widgets-addons plasma-nm plasma-pa plasma-dataengines-addons');
				$this->installPackages('khelpcenter kwalletmanager polkit-kde-agent-1 dolphin gwenview okular okular-extra-backends pinentry-qt');
				$this->installPackages('kate kcalc kde-spectacle ark falkon kcolorchooser kolourpaint kdeaccessibility xdg-desktop-portal-kde');
				$this->installPackages('krfb krename ksystemlog');
				$this->installPackages('kde-config-cron kde-config-systemd kde-config-tablet kde-config-plymouth kde-config-gtk-style-preview');

				if (!$this->isSelected(Feature::VM_INSTALL)) {
					$this->installPackages('konversation ktorrent k3b skanlite partitionmanager');
				}

				if ($this->isSelected(Feature::LUXEMBOURG)) {
					$this->installPackages('kde-l10n-de kde-l10n-engb kde-l10n-fr');
				}
			}

			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installPackages('cups cups-browsed cups-bsd cups-pk-helper printer-driver-gutenprint foomatic-db');
				$this->installPackages('network-manager sane xiccd jmtpfs mtp-tools xinput brightnessctl');
				$this->installPackages('bluez-cups bluez-obexd bluez-tools bluez-firmware firmware-linux');
				$this->installPackages('firmware-brcm80211 firmware-atheros firmware-intel-sound firmware-iwlwifi firmware-realtek');
				$this->installPackages('xserver-xephyr xnest');
				
				$this->installPackages('qemu-system bridge-utils vde2');

				// Needed by gparted / partitionmanager
				$this->installPackages('btrfs-progs f2fs-tools mtools jfsutils cryptsetup lvm2 reiser4progs reiserfsprogs udftools xfsprogs xfsdump nilfs-tools hfsutils hfsprogs');
			}
			break;

		case Feature::VNC:
			$this->installPackages('tigervnc-standalone-server tigervnc-xorg-extension tigervnc-scraping-server tigervnc-viewer');
			break;

		case Feature::MFC_L2710DW:
			// Printer driver ------------------------------------------
			$this->installPackages('printer-driver-brlaser');
			// Scanner driver ------------------------------------------
			$this->installFile(Location::LOCAL_FOLDER, 'debian/brother-mfc-l2710dw/brscan4-0.4.8-1.amd64.deb', '/tmp');
			$this->executeCommand('installDebFile /tmp/brscan4-0.4.8-1.amd64.deb');
			$this->executeCommand('${SED_CMD} "s/^SYSFS/#SYSFS/" ${CONFIX_PREFIX}/udev/rules.d/60-brother-brscan4-libsane-type1.rules');
			// Start scan using 'Scan' key
			$this->installFile(Location::LOCAL_FOLDER, 'debian/brother-mfc-l2710dw/brscan-skey-0.2.4-1.amd64.deb', '/tmp');
			$this->executeCommand('installDebFile /tmp/brscan-skey-0.2.4-1.amd64.deb');
			break;

		case Feature::ET2700:
			// Scanner driver ------------------------------------------
			// These packages are 'imagescan' dependencies
			$this->installPackages('graphicsmagick libboost-program-options1.67.0');
			$this->installFile(Location::LOCAL_FOLDER, 'debian/epson-et-2700/imagescan_3.62.0-1epson4debian10_amd64.deb', '/tmp');
			$this->executeCommand('installDebFile /tmp/imagescan_3.62.0-1epson4debian10_amd64.deb');
			$this->installFile(Location::LOCAL_FOLDER, 'debian/epson-et-2700/imagescan-plugin-networkscan_1.1.3-1epson4debian10_amd64.deb', '/tmp');
			$this->executeCommand('installDebFile /tmp/imagescan-plugin-networkscan_1.1.3-1epson4debian10_amd64.deb');
			$this->installFile(Location::LOCAL_FOLDER, 'debian/epson-et-2700/imagescan-plugin-ocr-engine_1.0.2-1epson4debian10_amd64.deb', '/tmp');
			$this->executeCommand('installDebFile /tmp/imagescan-plugin-ocr-engine_1.0.2-1epson4debian10_amd64.deb');
			break;

		case Feature::XRDP:
			$this->installPackages('xrdp');
			break;

		case Feature::SAMBA:
			$this->installPackages('samba');
			break;

		case Feature::DISPLAY_MANAGER:
			if ($this->isSelected(Feature::LXDM)) {
				$this->installPackages('lxdm');
			} else if ($this->isSelected(Feature::SDDM)) {
				$this->installPackages('sddm sddm-theme-breeze sddm-theme-elarun sddm-theme-maldives sddm-theme-maui sddm-theme-maya');
			} else if ($this->isSelected(Feature::LIGHTDM)) {
				$this->installPackages('lightdm lightdm-gtk-greeter');
			}
			break;

		case Feature::OFFICE_SUITE:
			$this->installPackages('libreoffice-writer libreoffice-calc libreoffice-impress libreoffice-math');
			$this->installPackages('libreoffice-avmedia-backend-gstreamer libreoffice-help-en-us mythes-en-us hyphen-en-us');
			$this->installPackages('gimp gimp-help-en');
			$this->installPackages('inkscape');

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installPackages('libreoffice-l10n-de libreoffice-l10n-en-gb libreoffice-l10n-fr');
				$this->installPackages('libreoffice-help-de libreoffice-help-en-gb libreoffice-help-fr');
				$this->installPackages('hyphen-de hyphen-en-gb hyphen-fr');
				$this->installPackages('mythes-de mythes-fr');
				$this->installPackages('gimp-help-de gimp-help-fr');
			}

			if ($this->isSelected(Feature::MATE)) {
				$this->installPackages('libreoffice-gtk3');
			}

			if ($this->isSelected(Feature::KDE)) {
				$this->installPackages('libreoffice-kde5 libreoffice-kf5 libreoffice-plasma libreoffice-style-breeze');
			}
			break;

		case Feature::THUNDERBIRD:
			$this->installPackages('thunderbird lightning');

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installPackages('thunderbird-l10n-de thunderbird-l10n-en-gb thunderbird-l10n-fr');
				$this->installPackages('lightning-l10n-de lightning-l10n-en-gb lightning-l10n-fr');
			}
			break;

		case Feature::FIREFOX:
			switch ($this->getFirefoxVersion()) {
			case Feature::FIREFOX_ESR:
				$this->installPackages('firefox-esr');

				if ($this->isSelected(Feature::LUXEMBOURG)) {
					$this->installPackages('firefox-esr-l10n-de firefox-esr-l10n-en-gb firefox-esr-l10n-fr');
				}
				break;

			case Feature::FIREFOX_LATEST:
				$this->installArchive(Location::REMOTE_URL, '$(mozillaProductURL firefox)', '${APPS_PREFIX}', 'tar.bz2');
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/firefox.desktop', '${APP_LAUNCHERS_DIR}');
				break;
			}
			break;

		case Feature::SCM:
			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installPackages('cvs subversion mercurial');
			}

			$this->installPackages('git');
			break;

		case Feature::CMAKE:
			$this->installPackages('cmake cmake-gui meson');
			break;

		case Feature::DEVELOP:
			$this->installPackages('g++ gdb manpages-posix manpages-posix-dev zeal');
			$this->installPackages('clang clang-tools lldb clang-format clang-tidy');
			$this->installPackages('codeblocks cscope cppcheck valgrind cccc');
			break;

		case Feature::OPENJDK:
			$this->installPackages('adoptopenjdk-8-hotspot');
			$this->installPackages('adoptopenjdk-11-hotspot');
			break;

		case Feature::NODEJS:
			$this->installPackages('nodejs');
			break;

		case Feature::VSCODE:
			$this->installPackages('code');
			break;

		case Feature::MYSQL:
			$this->installPackages('mariadb-server');
			break;

		case Feature::POSTGRESQL:
			$this->installPackages('postgresql-12');

			if ($this->isSelected(Feature::DEVELOP)) {
				$this->installPackages('pgadmin3 pgadmin4 pgbadger postgresql-doc-12 postgresql-contrib-12');
			}
			break;

		case Feature::APACHE_PHP:
			$this->installPackages('apache2 libapache2-mod-php libapache2-mod-authnz-external');
			$this->installPackages('php-pgsql php-xml php-mbstring php-gd php-zip');
			$this->installPackages('composer php-imagick php-intl php-pear php-xdebug');
			break;

		case Feature::ECLIPSE:
			$this->installArchive(Location::REMOTE_URL, '$(eclipseURL)', '${APPS_PREFIX}', 'tar.gz');
			$this->installFile(Location::LOCAL_FOLDER, 'icons/eclipse.svg', '${APP_ICONS_DIR}');
			$this->installConfig(Location::LOCAL_FOLDER, 'desktop/eclipse.desktop', '${APP_LAUNCHERS_DIR}');
			break;

		case Feature::VBOX_HOST:
			$this->installPackages('virtualbox-6.1');
			break;

		case Feature::VMWARE_GUEST:
			$this->installPackages('open-vm-tools');

			if ($this->isSelected(Feature::DESKTOP)) {
				$this->installPackages('open-vm-tools-desktop');
			}
			break;

		case Feature::QEMU_GUEST:
			$this->installPackages('qemu-guest-agent');
			break;

		case 'vbox-extpack':
			// Attempt to automate extension pack installation, not working yet.
			$this->installArchive(Location::REMOTE_URL, '$(virtualboxExtPackURL)', '$(virtualboxExtPackDir)', 'tar.gz');
			break;

		case 'foliate':
			$this->executeScriptlet(DebianInfo::ID . '/foliate-init');
			$this->installFile(Location::REMOTE_URL, '$(foliateURL)', '/tmp');
			$this->installFile(Location::LOCAL_FOLDER, 'icons/com.github.johnfactotum.Foliate.svg', '${APP_ICONS_DIR}');
			$this->executeCommand('installDebFile /tmp/${FOLIATE_PACKAGE}');
			break;
		}

		if ($propagate) {
			parent::installFeature($featureId);
		}
	}

	public function completeFeature($featureId) {
		parent::completeFeature($featureId);

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::completeFeature($featureId)'\n");
		}

		switch ($featureId) {
		case Feature::CONSOLE:
			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->executeScriptlet(DebianInfo::ID . '/luxembourg');
			}
			break;

		case Feature::DESKTOP:
			if ($this->isSelected(Feature::KDE)) {
				$this->executeCommand("echo 'export GTK_USE_PORTAL=1' >> /etc/profile.d/local.sh");
			}
			break;
		}
	}
}
