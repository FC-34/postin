<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'BaseBSDInstaller.php';
require_once 'FreeBSDInfo.php';

class FreeBSD extends BaseBSDInstaller {
	public function __construct() {
		parent::__construct();
		$this->info = new FreeBSDInfo();
		$this->mergeFeatures([
			new FeatureChoice(FreeBSDInfo::ID, Feature::LATEST, 'Use most recent packages ("latest" instead of "quaterly")'),
			new FeatureChoice(FreeBSDInfo::ID, 'intel', 'Support for Intel i915 and HDA'),
			new FeatureChoice(FreeBSDInfo::ID, Feature::VBOX_HOST, 'Install VirtualBox host'),
			new FeatureChoice(FreeBSDInfo::ID, Feature::VBOX_GUEST, 'Run as VirtualBox guest'),
			new FeatureChoice(FreeBSDInfo::ID, Feature::VMWARE_GUEST, 'Run as VMWare guest'),
			new FeatureChoice(FreeBSDInfo::ID, Feature::QEMU_GUEST, 'Run as QEMU guest'),
			new FeatureChoice(FreeBSDInfo::ID, 'sources', 'Install / update base and ports sources'),
		]);
	}

	public function configureInstaller() {
		parent::configureInstaller();
		
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInstaller()'\n");
		}

		$this->executeScriptlet(FreeBSDInfo::ID . '/00-required');
		$this->executeScriptlet(FreeBSDInfo::ID . '/01-specific');
		$this->executeScriptlet(FreeBSDInfo::ID . '/02-packages');
	}

	public function configureInitialInstallation() {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInitialInstallation()'\n");
		}

		$this->executeScriptlet(FreeBSDInfo::ID . '/10-initial-install');
	}

	public function installFeature($featureId) {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::installFeature($featureId)'\n");
		}

		$propagate = TRUE;

		switch ($featureId) {
		case Feature::CONSOLE:
			$this->installPackages('p7zip zip neovim rsync doas py37-pip pwgen');
			//$this->installPackages('ipmitool smartmontools');
			//$this->installPackages('bastille');
			//$this->installPackages('bsdstats');
			break;

		case Feature::DESKTOP:
			$this->installPackages('xorg-minimal xf86-input-libinput xf86-input-evdev');
			$this->installPackages('avahi-app nss_mdns xdg-user-dirs freedesktop-sound-theme');

			if ($this->isSelected(Feature::VM_INSTALL)) {
				$this->installPackages('xf86-video-scfb');
				$this->installConfig(Location::LOCAL_FOLDER, 'freebsd/20-scfb.conf', '${CONFIG_PREFIX}/X11/xorg.conf.d');
			}

			if ($this->isSelected(Feature::MATE)) {
				$this->installPackages('mate webp-pixbuf-loader seahorse');
				$this->installPackages('geany geany-plugins dconf-editor galculator');
				$this->installPackages('numix-gtk-theme numix-icon-theme');
				
				if (!$this->isSelected(Feature::VM_INSTALL)) {
					$this->installPackages('transmission-gtk hexchat liferea');
					$this->installPackages('system-config-printer gscan2pdf');
				}
			}
			
			// On FreeBSD, the LXQT port installs several KDE dependencies, so why not use KDE instead?

			if ($this->isSelected(Feature::KDE)) {
				$this->installPackages('plasma5-plasma');
				$this->installPackages('konsole dolphin khelpcenter kdialog okular gwenview kate keditbookmarks');
				$this->installPackages('ktimer kdf kdebugsettings filelight kwalletmanager kcalc kcharselect ark');
				$this->installPackages('ksystemlog kcron krfb krdc falkon');
				$this->installPackages('spectacle kolourpaint kdegraphics-svgpart kcolorchooser kruler');
				$this->installPackages('kmag kompare krename');
				
				if (!$this->isSelected(Feature::VM_INSTALL)) {
					$this->installPackages('konversation ktorrent print-manager skanlite');
				}
			}

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/10-keyboard_ch-fr.conf', '${CONFIG_PREFIX}/X11/xorg.conf.d');
			}

			$this->installPackages('xorg-nestserver xephyr hal');
			$this->installPackages('gstreamer1-plugins-core gstreamer1-plugins-good gstreamer1-plugins-bad');
			$this->installPackages('sane-backends pdftk');
			$this->installPackages('cups gutenprint cups-filters');
			$this->installPackages('webcamd webcamoid');
			$this->installPackages('exif base64');
			$this->installPackages('fusefs-ntfs-compression fusefs-jmtpfs fusefs-sshfs');
			break;

		case Feature::DISPLAY_MANAGER:
			if ($this->isSelected(Feature::SDDM)) {
				$this->installPackages('sddm');
			} else if ($this->isSelected(Feature::LIGHTDM)) {
				$this->installPackages('lightdm-gtk-greeter');
			}
			break;

		case Feature::OFFICE_SUITE:
			$this->installPackages('gimp');
			$this->installPackages('inkscape');
			$this->installPackages('apache-openoffice');

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installPackages('fr-hunspell en-hunspell de-hunspell');
				$this->installPackages('fr-hyphen de-hyphen');
				// If using libreoffice, also install:
				//$this->installPackages('de-libreoffice en_GB-libreoffice fr-libreoffice lb-libreoffice');
			}
			break;

		case Feature::FIREFOX:
			switch ($this->getFirefoxVersion()) {
			case Feature::FIREFOX_ESR:
				$this->installPackages('firefox-esr');
				break;

			case Feature::FIREFOX_LATEST:
				$this->installPackages('firefox');
				break;
			}
			break;

		case Feature::THUNDERBIRD:
			$this->installPackages('thunderbird');
			break;

		case Feature::IM_CLIENT:
			$this->installPackages('pidgin pidgin-libnotify pidgin-otr pidgin-sipe pidgin-skypeweb purple-hangouts telegram-purple');
			break;

		case 'foliate':
			$this->installPackages('foliate');
			$this->installConfig(Location::LOCAL_FOLDER, 'desktop/com.github.johnfactotum.Foliate.desktop', '${APP_LAUNCHERS_DIR}');
			$this->installFile(Location::LOCAL_FOLDER, 'icons/com.github.johnfactotum.Foliate.svg', '${APP_ICONS_DIR}');
			break;

		case Feature::MFC_L2710DW:
			// Printer driver
			$this->installPackages('brlaser');
			break;

		case Feature::ET2700:
			// Printer driver
			$this->installPackages('epson-inkjet-printer-escpr');
			break;

		case Feature::VNC:
			$this->installPackages('tigervnc-server tigervnc-viewer');
			break;

		case Feature::XRDP:
			$this->installPackages('xrdp');
			break;

		case Feature::SAMBA:
			$this->installPackages('samba410');
			break;

		case Feature::CALIBRE:
			$this->installPackages('calibre');
			break;

		case Feature::POSTGRESQL:
			$this->installPackages('postgresql12-server');

			if ($this->isSelected(Feature::DEVELOP)) {
				$this->installPackages('postgresql12-docs postgresql12-contrib pgadmin3 pgbadger');
			}
			break;

		case Feature::NODEJS:
			$this->installPackages('node npm');
			break;

		case Feature::OPENJDK:
			$this->installPackages('openjdk8');
			$this->installPackages('openjdk11');
			break;

		case Feature::ECLIPSE:
			$this->installPackages('eclipse subversive');
			$this->installFile(Location::LOCAL_FOLDER, 'icons/eclipse.svg', '${APP_ICONS_DIR}');
			$this->installConfig(Location::LOCAL_FOLDER, 'desktop/eclipse.desktop', '${APP_LAUNCHERS_DIR}');
			break;

		case Feature::VSCODE:
			$this->installPackages('vscode');
			break;

		case Feature::SCM:
			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installPackages('cvs mercurial');
			}

			$this->installPackages('git');
			break;

		case Feature::TCL_TK:
			$this->installPackages('tcl86 tk86');
			break;

		case Feature::CMAKE:
			$this->installPackages('cmake cmake-gui meson');
			$this->installFile(Location::LOCAL_FOLDER, 'icons/cmake.svg', '${APP_ICONS_DIR}', 'CMakeSetup.svg');
			break;

		case Feature::DEVELOP:
			$this->installPackages('pkgconf gnulib getopt gdb zeal');
			$this->installPackages('llvm11');
			$this->installPackages('codeblocks cscope cppcheck valgrind cccc');
			$this->installPackages('portlint');
			break;

		case Feature::APACHE_PHP:
			$this->installPackages('apache24 mod_php74');
			$this->installPackages('php74-pdo_pgsql php74-zip php74-extensions');
			$this->installPackages('php74-composer php74-pear php74-pecl-uuid php74-readline php74-pecl-xdebug');
			break;

		case 'intel':
			$this->executeScriptlet(FreeBSDInfo::ID . '/inteldrm');
			$this->executeScriptlet(FreeBSDInfo::ID . '/intelhda');
			break;

		case 'sources':
			$this->executeCommand('checkSourcesAndTools');
			break;

		case Feature::VBOX_HOST:
			$this->installPackages('virtualbox-ose');
			break;

		case Feature::VBOX_GUEST:
			$this->installPackages('virtualbox-ose-additions');
			$this->installConfig(Location::LOCAL_FOLDER, 'desktop/90-vboxguest.fdi', '${INSTALL_PREFIX}/etc/hal/fdi/policy');
			$this->installConfig(Location::LOCAL_FOLDER, 'desktop/15-vboxmouse.conf', '${CONFIG_PREFIX}/X11/xorg.conf.d');
			break;

		case Feature::VMWARE_GUEST:
			$this->installPackages('open-vm-tools');
			break;

		case Feature::QEMU_GUEST:
			# Strangely, qemu-ga (GUEST agent) is part of the qemu (hypervisor HOST) package...
			$this->installPackages('qemu');
			break;
		}

		if ($propagate) {
			parent::installFeature($featureId);
		}
	}

	public function completeFeature($featureId) {
		parent::completeFeature($featureId);

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::completeFeature($featureId)'\n");
		}

		switch ($featureId) {
		case Feature::CONSOLE:
			$this->executeScriptlet(FreeBSDInfo::ID . '/console');
			$this->executeScriptlet(FreeBSDInfo::ID . '/bastille');
			$this->executeScriptlet(FreeBSDInfo::ID . '/bsdstats');

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->executeScriptlet(FreeBSDInfo::ID . '/luxembourg');
			}
			break;

		case Feature::DISPLAY_MANAGER:
			if ($this->isSelected(Feature::SDDM)) {
				$this->executeScriptlet(DragonFlyBSDInfo::ID . '/sddm');
			} else if ($this->isSelected(Feature::LIGHTDM)) {
				$this->executeScriptlet(DragonFlyBSDInfo::ID . '/lightdm');
			}
			break;

		case Feature::DESKTOP:
			$this->executeScriptlet(FreeBSDInfo::ID . '/desktop');

			if (!$this->isSelected(Feature::VM_INSTALL)) {
//				$this->executeScriptlet(FreeBSDInfo::ID . '/exfat');
				$this->executeScriptlet(FreeBSDInfo::ID . '/hal-cfg');
			}
			break;

		case Feature::OFFICE_SUITE:
			$this->executeScriptlet(FreeBSDInfo::ID . '/switch-office-suite');
			break;

		case Feature::ECLIPSE:
			$this->executeScriptlet(FreeBSDInfo::ID . '/eclipse-complete');
			break;

		case Feature::DEVELOP:
			$this->executeScriptlet(FreeBSDInfo::ID . '/devel');
			break;

		case Feature::FIREFOX:
			$this->executeScriptlet(FreeBSDInfo::ID . '/firefox');
			break;

		case Feature::VBOX_HOST:
			$this->executeScriptlet(FreeBSDInfo::ID . '/vboxhost');
			break;

		case Feature::VBOX_GUEST:
			$this->executeScriptlet(FreeBSDInfo::ID . '/vboxguest');
			break;
		}
	}
}
