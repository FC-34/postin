<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'BaseUnixInstaller.php';
require_once 'BaseLinuxInfo.php';

abstract class BaseLinuxInstaller extends BaseUnixInstaller {
	public function configureInstaller() {
		parent::configureInstaller();
		
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInstaller()'\n");
		}
		
		$this->executeScriptlet(BaseLinuxInfo::ID . '/00-required');
		$this->executeScriptlet(BaseLinuxInfo::ID . '/01-specific');
	}
	
	public function installFeature($featureId) {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::installFeature($featureId)'\n");
		}
		
		$propagate = TRUE;
		
		switch ($featureId) {
		case Feature::CALIBRE:
			$this->executeScriptlet(BaseLinuxInfo::ID . '/calibre-install');
			break;
		
		case Feature::DEVELOP:
			$this->installPackages('gcc make autoconf autoconf-archive automake libtool pkg-config');
			break;
		}
		
		if ($propagate) {
			parent::installFeature($featureId);
		}
	}
	
	public function completeFeature($featureId) {
		parent::completeFeature($featureId);
		
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::completeFeature($featureId)'\n");
		}
		
		switch ($featureId) {
		case Feature::CONSOLE:
			$this->executeScriptlet(BaseLinuxInfo::ID . '/console');
			break;
		
		case Feature::SAMBA:
			break;
		
		case Feature::ECLIPSE:
			$this->executeScriptlet(BaseLinuxInfo::ID . '/eclipse-complete');
			break;
		}
	}
}
