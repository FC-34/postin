<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'BaseBSDInfo.php';

class DragonFlyBSDInfo extends BaseBSDInfo {
	const ID = 'dragonflybsd';
	
	public function getId() {
		return self::ID;
	}
	
	public function getSystemName() {
		return 'DragonFlyBSD';
	}
	
	public function getVersionNumber() {
		return '5.8.3';
	}

	public function getHttpClientCommand() {
		return 'fetch -q -o';
	}

	public function getSedInlineCommand() {
		return "sed -i ''";
	}
	
	public function getDesktopEnvironments() {
		return [ Feature::MATE, Feature::KDE ];
	}
	
	public function getDisplayManagers() {
		return [ Feature::LIGHTDM, Feature::SDDM ];
	}
	
	public function getHWInstallImage() {
		return 'dfly-x86_64-' . $this->getVersionNumber() . '_REL.img';
	}
	
	public function getVMInstallImage() {
		return 'dfly-x86_64-' . $this->getVersionNumber() . '_REL.iso';
	}
}
