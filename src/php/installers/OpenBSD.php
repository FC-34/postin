<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'BaseBSDInstaller.php';
require_once 'OpenBSDInfo.php';

class OpenBSD extends BaseBSDInstaller {
	public function __construct() {
		parent::__construct();
		$this->info = new OpenBSDInfo();
		$this->mergeFeatures([
			new FeatureChoice(OpenBSDInfo::ID, Feature::LATEST, 'Use most recent packages ("packages" instead of "packages-stable")'),
			new FeatureChoice(OpenBSDInfo::ID, Feature::QEMU_GUEST, 'Run as QEMU guest'),
		]);
	}

	public function configureInstaller() {
		parent::configureInstaller();

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInstaller()'\n");
		}

		$this->executeScriptlet(OpenBSDInfo::ID . '/00-required');
		$this->executeScriptlet(OpenBSDInfo::ID . '/02-packages');
	}

	public function configureInitialInstallation() {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInitialInstallation()'\n");
		}

		$this->executeScriptlet(OpenBSDInfo::ID . '/10-initial-install');
	}

	public function configureFeature($featureId) {
		parent::configureFeature($featureId);

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureFeature($featureId)'\n");
		}

		switch ($featureId) {
		case Feature::DESKTOP:
			if ($this->isSelected(Feature::MATE)) {
				$this->configureFeature(Feature::NUMIX);
			}
			break;
		}
	}

	public function installFeature($featureId) {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::installFeature($featureId)'\n");
		}

		$propagate = TRUE;

		switch ($featureId) {
		case Feature::CONSOLE:
			$this->installPackages('neovim rsync--iconv zip bzip2 p7zip py3-pip pwgen');
			//$this->installPackages('ipmitool smartmontools');
			$this->executeCommand("ln -s $(find ${INSTALL_PREFIX}/bin -name 'pip3*' -print) ${INSTALL_PREFIX}/bin/pip");
			break;

		case Feature::DESKTOP:
			if ($this->isSelected(Feature::MATE)) {
				$this->installPackages('mate'); // check: eom engrampa
				$this->installPackages('galculator caja-extensions');
				$this->installPackages('geany dconf-editor gnome-keyring seahorse');
				
				if (!$this->isSelected(Feature::VM_INSTALL)) {
					$this->installPackages('transmission-gtk hexchat liferea');
					$this->installPackages('system-config-printer');
				}
				
				$this->installFeature(Feature::NUMIX);
			}

			if ($this->isSelected(Feature::LXQT)) {
				$this->installPackages('lxqt-main lxqt-extras');
				$this->installPackages('xpdf featherpad');
				// Missing: lxqt-archiver fbreader quassel cutecom quiterss kColorPicker kadu
				
				if (!$this->isSelected(Feature::VM_INSTALL)) {
					$this->installPackages('qbittorrent xscreensaver');
				}
				
				$this->installFeature(Feature::NUMIX);
			}

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/10-keyboard_ch-fr.conf', '${CONFIG_PREFIX}/X11/xorg.conf.d');
			}

			$this->installPackages('gstreamer1-plugins-good gstreamer1-plugins-bad gstreamer1-plugins-ugly');
			$this->installPackages('sound-theme-freedesktop xdg-user-dirs');
			
			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installPackages('sane-backends--');
				$this->installPackages('cups gutenprint');
			}
			break;

		case Feature::DISPLAY_MANAGER:
			if ($this->isSelected(Feature::XDM)) {
				// TODO
			}
			break;

		case Feature::OFFICE_SUITE:
			$this->installPackages('libreoffice');
			$this->installPackages('inkscape');
			$this->installPackages('gimp');

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installPackages('libreoffice-i18n-de libreoffice-i18n-fr');
			}
			break;

		case Feature::IM_CLIENT:
			$this->installPackages('pidgin--main pidgin-libnotify pidgin-otr pidgin-sipe');
			break;

		case 'foliate':
			$this->installPackages('foliate');
			break;

		case Feature::FIREFOX:
			switch ($this->getFirefoxVersion()) {
			case Feature::FIREFOX_ESR:
				$this->installPackages('firefox-esr');
				break;

			case Feature::FIREFOX_LATEST:
				$this->installPackages('firefox');
				break;
			}
			break;

		case Feature::THUNDERBIRD:
			$this->installPackages('thunderbird');

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installPackages('thunderbird-i18n-de thunderbird-i18n-en-GB thunderbird-i18n-fr');
			}
			break;

		case Feature::VNC:
			$this->installPackages('tigervnc');
			break;

		case Feature::MFC_L2710DW:
			// Printer driver
			$this->installPackages('brlaser');
			break;

		case Feature::ET2700:
			// Printer driver
			$this->installPackages('epson-inkjet-printer-escpr');
			break;

		case Feature::POSTGRESQL:
			$this->installPackages('postgresql-server');

			if ($this->isSelected(Feature::DEVELOP)) {
				$this->installPackages('pgadmin3 pgbadger postgresql-docs');
			}
			break;

		case Feature::MYSQL:
			$this->installPackages('mariadb-server');
			break;

		case Feature::SAMBA:
			$this->installPackages('samba');
			break;

		case Feature::OPENJDK:
			$this->installPackages('jdk%1.8');
			$this->installPackages('jdk%11');
			break;

		case Feature::NODEJS:
			$this->installPackages('node');
			break;

		case Feature::SCM:
			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installPackages('subversion mercurial');
			}

			$this->installPackages('git');
			break;

		case Feature::CMAKE:
			$this->installPackages('cmake meson');
			break;

		case Feature::TCL_TK:
			$this->installPackages('tcl%8.6 tk%8.6');
			break;

		case Feature::DEVELOP:
			$this->installPackages('pkgconf gdb zeal');
			$this->installPackages('llvm clang-tools-extra');
			$this->installPackages('codeblocks cscope cppcheck valgrind');
			break;

		case Feature::APACHE_PHP:
			$this->installPackages('apache-httpd php%7.4 composer');
			break;

		case Feature::QEMU_GUEST:
			// qemu-ga is part of the full QEMU package... :(
			$this->installPackages('qemu');
			break;
		}

		if ($propagate) {
			parent::installFeature($featureId);
		}
	}

	public function completeFeature($featureId) {
		parent::completeFeature($featureId);

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::completeFeature($featureId)'\n");
		}

		switch ($featureId) {
		case Feature::DESKTOP:
			if ($this->isSelected(Feature::MATE)) {
				$this->completeFeature(Feature::NUMIX);
			}
			break;

		case Feature::DISPLAY_MANAGER:
			if ($this->isSelected(Feature::XDM)) {
				// TODO configure xenodm - at the very least, 
				// add a "xenodm_flags=" line to /etc/rc.conf.local
			}
			break;
		}
	}
}
