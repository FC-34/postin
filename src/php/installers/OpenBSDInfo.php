<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'BaseBSDInfo.php';

class OpenBSDInfo extends BaseBSDInfo {
	const ID = 'openbsd';
	
	public function getId() {
		return self::ID;
	}
	
	public function getSystemName() {
		return 'OpenBSD';
	}
	
	public function getVersionNumber() {
		return '6.8';
	}

	public function getHttpClientCommand() {
		return 'ftp -V -o';
	}

	public function getSystemConfigurationFile() {
		return '/etc/rc.conf.local';
	}
	
	public function getDesktopEnvironments() {
		return [ Feature::MATE, Feature::LXQT ];
	}
	
	public function getDisplayManagers() {
		return [ Feature::XDM ];
	}
	
	private function getVersionSuffix() {
		return str_replace('.', '', $this->getVersionNumber());
	}
	
	public function getHWInstallImage() {
		return 'install' . $this->getVersionSuffix() . '.img';
	}
	
	public function getVMInstallImage() {
		return 'install' . $this->getVersionSuffix() . '.iso';
	}
}
