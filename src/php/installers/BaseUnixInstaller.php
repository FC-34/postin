<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'BaseInstaller.php';
require_once 'BaseUnixInfo.php';

abstract class BaseUnixInstaller extends BaseInstaller {
	public function __construct() {
		parent::__construct();
		$this->mergeProfiles([
			new ProfileChoice(BaseUnixInfo::ID, 'console', 'Console (e.g. server)', [
				Feature::CONSOLE,
			]),
			new ProfileChoice(BaseUnixInfo::ID, 'desktop', 'General-purpose desktop', [
				Feature::CONSOLE,
				Feature::DESKTOP,
			]),
			new ProfileChoice(BaseUnixInfo::ID, 'develop', 'Development workstation', [
				Feature::CONSOLE,
				Feature::DESKTOP,
				Feature::DEVELOP,
			]),
		]);
	}
	
	public function getApplicationsPrefix() {
		return '${INSTALL_PREFIX}/lib';
	}
	
	public function getAppIconsDir() {
		return '${INSTALL_PREFIX}/share/pixmaps';
	}
	
	public function getAppLaunchersDir() {
		return '${INSTALL_PREFIX}/share/applications';
	}

	public function getFirefoxVersion() {
		return Feature::FIREFOX_ESR;
	}

	public function configureInstaller() {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInstaller()'\n");
		}

		$this->executeScriptlet(BaseUnixInfo::ID . '/00-stringlib');
		$this->executeScriptlet(BaseUnixInfo::ID . '/01-specific');
	}

	public function configureFeature($featureId) {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureFeature($featureId)'\n");
		}

		switch ($featureId) {
		case Feature::DESKTOP:
			$this->configureFeature(Feature::DISPLAY_MANAGER);
			$this->configureFeature(Feature::FIREFOX);
			$this->configureFeature(Feature::OFFICE_SUITE);

			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->configureFeature(Feature::MEDIA_PLAYER);
			}
			break;

		case Feature::OFFICE_SUITE:
			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->configureFeature(Feature::THUNDERBIRD);
			}
			break;

		case Feature::SQL_WORKBENCH:
			$this->executeScriptlet(BaseUnixInfo::ID . '/sqlworkbench-init');
			break;
		}
	}

	public function installFeature($featureId) {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::installFeature($featureId)'\n");
		}

		switch ($featureId) {
		case Feature::CONSOLE:
			$this->installArchive(Location::LOCAL_FOLDER, 'packages/local-tools', '${JAVA_PREFIX}');
			$this->installArchive(Location::LOCAL_FOLDER, 'packages/jdbc-drivers', '${JAVA_PREFIX}');
			break;
		
		case Feature::DESKTOP:
			if ($this->isSelected(Feature::MATE)) {
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/mate/98_dconf-editor.gschema.override', '${INSTALL_PREFIX}/share/glib-2.0/schemas');
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/mate/98_nmapplet.gschema.override', '${INSTALL_PREFIX}/share/glib-2.0/schemas');
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/mate/98_mate.gschema.override', '${INSTALL_PREFIX}/share/glib-2.0/schemas');
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/mate/mate-mimeapps.list', '${APP_LAUNCHERS_DIR}');
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/mate/default.layout', '${INSTALL_PREFIX}/share/mate-panel/layouts');
			}

			if ($this->isSelected(Feature::LXQT)) {
				$this->installArchive(Location::LOCAL_FOLDER, 'desktop/lxqt/lxqt-skel', '/etc/skel');
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/lxqt/lxqt-mimeapps.list', '${APP_LAUNCHERS_DIR}');
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/lxqt/panel.conf', '/etc/skel/.config/lxqt');
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/lxqt/settings.conf', '/etc/skel/.config/pcmanfm-qt/lxqt');
			}

			if ($this->isSelected(Feature::KDE)) {
				$this->installArchive(Location::LOCAL_FOLDER, 'desktop/kde/kde-skel', '/etc/skel/.config');
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/kde/kde-mimeapps.list', '${APP_LAUNCHERS_DIR}');
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/kde/kde_settings.conf', '/etc/sddm.conf.d');
			}

			$this->installConfig(Location::LOCAL_FOLDER, 'desktop/98_filechooser.gschema.override', '${INSTALL_PREFIX}/share/glib-2.0/schemas');
			$this->installConfig(Location::LOCAL_FOLDER, 'desktop/98-personal-computer.pkla', '${CONFIG_PREFIX}/polkit-1/localauthority/50-local.d');
			$this->installConfig(Location::LOCAL_FOLDER, 'desktop/98-personal-computer.rules', '${CONFIG_PREFIX}/polkit-1/rules.d');
			$this->installArchive(Location::LOCAL_FOLDER, 'packages/vegetal-dreams-leaves', '${INSTALL_PREFIX}/share');
			$this->installArchive(Location::LOCAL_FOLDER, 'packages/vegetal-dreams-raindrops', '${INSTALL_PREFIX}/share');

			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installArchive(Location::LOCAL_FOLDER, 'packages/epubcheck-4.2.2', '${JAVA_PREFIX}');
				$this->installArchive(Location::LOCAL_FOLDER, 'packages/qrcodegen-1.14.2', '${JAVA_PREFIX}');
				$this->installArchive(Location::LOCAL_FOLDER, 'packages/pdfbox-app-2.0.22', '${JAVA_PREFIX}');
			}

			$this->installFeature(Feature::DISPLAY_MANAGER);
			$this->installFeature(Feature::FIREFOX);
			$this->installFeature(Feature::OFFICE_SUITE);

			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installFeature(Feature::MEDIA_PLAYER);
			}
			break;

		case Feature::OFFICE_SUITE:
			if (!$this->isSelected(Feature::VM_INSTALL)) {
				if ($this->isSelected(Feature::KDE)) {
					$this->installFeature(Feature::KONTAKT);
				} else {
					$this->installFeature(Feature::THUNDERBIRD);
				}
			}
			
			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installArchive(Location::LOCAL_FOLDER, 'packages/hunspell-lb_LU', '${INSTALL_PREFIX}/share/hunspell');
			}
			
			$this->installConfig(Location::LOCAL_FOLDER, 'libreoffice/disable-file-locking.xcd', '${INSTALL_PREFIX}/lib/libreoffice/share/registry');
			break;

		case Feature::MEDIA_PLAYER:
			$this->installPackages('vlc');
			break;

		case Feature::SOAPUI:
			$this->installArchive(Location::LOCAL_FOLDER, 'packages/soapui-5.6.0', '${JAVA_PREFIX}');
			break;

		case Feature::SQL_WORKBENCH:
			$this->installArchive(Location::LOCAL_FOLDER, 'packages/sqlworkbench-127', '${JAVA_PREFIX}');
			break;

		case Feature::NUMIX:
			$this->installArchive(Location::LOCAL_FOLDER, 'packages/numix-themes', '${INSTALL_PREFIX}/share');
			break;

		case Feature::DEVELOP:
			$this->installFeature(Feature::SCM);
			break;
		}
	}

	public function completeFeature($featureId) {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::completeFeature($featureId)'\n");
		}

		switch ($featureId) {
		case Feature::CONSOLE:
			$this->executeScriptlet(BaseUnixInfo::ID . '/console-complete');
			$this->executeScriptlet(BaseUnixInfo::ID . '/paper-complete');
			break;
		
		case Feature::DESKTOP:
			$this->completeFeature(Feature::DISPLAY_MANAGER);
			$this->completeFeature(Feature::FIREFOX);
			$this->completeFeature(Feature::OFFICE_SUITE);

			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->completeFeature(Feature::MEDIA_PLAYER);
			}

			if ($this->isSelected(Feature::MATE)) {
				$this->executeScriptlet(BaseUnixInfo::ID . '/mate-complete');
				
				if ($this->isSelected(Feature::VM_INSTALL)) {
					$this->executeScriptlet(BaseUnixInfo::ID . '/mate-vm-complete');;
				}
			}
			
			$this->executeScriptlet(BaseUnixInfo::ID . '/desktop-complete');
			break;

		case Feature::OFFICE_SUITE:
			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->completeFeature(Feature::THUNDERBIRD);
			}
			break;

		case Feature::MFC_L2710DW:
			$this->executeScriptlet(BaseUnixInfo::ID . '/mfc-l2710dw-complete');
			break;

		case Feature::ET2700:
			$this->executeScriptlet(BaseUnixInfo::ID . '/et2700-complete');
			break;

		case Feature::NUMIX:
			$this->executeScriptlet(BaseUnixInfo::ID . '/numix-complete');
			break;

		case Feature::XRDP:
			$this->executeScriptlet(BaseUnixInfo::ID . '/xrdp-complete');
			break;

		case Feature::DISPLAY_MANAGER:
			if ($this->isSelected(Feature::LXDM)) {
				$this->executeScriptlet(BaseUnixInfo::ID . '/lxdm-complete');
			} else if ($this->isSelected(Feature::SDDM)) {
				$this->executeScriptlet(BaseUnixInfo::ID . '/sddm-complete');
			} else if ($this->isSelected(Feature::LIGHTDM)) {
				$this->executeScriptlet(BaseUnixInfo::ID . '/lightdm-complete');
			}
			break;

		case Feature::FIREFOX:
			$this->installFile(Location::LOCAL_FOLDER, 'mozilla/firefox-extensions.tgz', '/tmp');
			$this->installConfig(Location::LOCAL_FOLDER, 'mozilla/all-users-firefox.js', '/tmp');
			$this->installConfig(Location::LOCAL_FOLDER, 'mozilla/all-users-firefox.cfg', '/tmp');
			$this->executeScriptlet(BaseUnixInfo::ID . '/firefox-complete');
			break;

		case Feature::THUNDERBIRD:
			$this->installArchive(Location::LOCAL_FOLDER, 'mozilla/thunderbird-extensions', '${APPS_PREFIX}/thunderbird/extensions');
			$this->installConfig(Location::LOCAL_FOLDER, 'mozilla/all-users-thunderbird.js', '${APPS_PREFIX}/thunderbird/defaults/pref');
			$this->executeScriptlet(BaseUnixInfo::ID . '/thunderbird-complete');
			break;

		case Feature::SQL_WORKBENCH:
			$this->executeScriptlet(BaseUnixInfo::ID . '/sqlworkbench-complete');
			break;
		}
	}

	public function completeInstallation() {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::completeInstallation()'\n");
		}

		$this->executeScriptlet(BaseUnixInfo::ID . '/launchers');
		$this->installLauncherIfMissing('xcalc', Location::LOCAL_FOLDER, 'desktop/xcalc.desktop');
		$this->installLauncherIfMissing('xpdf', Location::LOCAL_FOLDER, 'desktop/xpdf.desktop');
		$this->installLauncherIfMissing('cutecom', Location::LOCAL_FOLDER, 'desktop/cutecom.desktop');
		$this->executeScriptlet(BaseUnixInfo::ID . '/90-finish');
	}
}
