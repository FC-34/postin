<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'BaseLinuxInstaller.php';
require_once 'VoidLinuxInfo.php';

class VoidLinux extends BaseLinuxInstaller {
	public function __construct() {
		parent::__construct();
		$this->info = new VoidLinuxInfo();
		$this->mergeFeatures([
			new FeatureChoice(VoidLinuxInfo::ID, Feature::VBOX_HOST, 'Install VirtualBox host'),
			new FeatureChoice(VoidLinuxInfo::ID, Feature::VBOX_GUEST, 'Run as VirtualBox guest'),
			new FeatureChoice(VoidLinuxInfo::ID, Feature::VMWARE_GUEST, 'Run as VMWare guest'),
			new FeatureChoice(VoidLinuxInfo::ID, Feature::QEMU_GUEST, 'Run as QEMU guest'),
		]);
	}

	public function configureInstaller() {
		parent::configureInstaller();

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInstaller()'\n");
		}

		$this->executeScriptlet(VoidLinuxInfo::ID . '/01-specific');
	}

	public function configureInitialInstallation() {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureInitialInstallation()'\n");
		}

		$this->executeScriptlet(VoidLinuxInfo::ID . '/10-initial-install');
	}

	public function configureFeature($featureId) {
		parent::configureFeature($featureId);

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::configureFeature($featureId)'\n");
		}

		switch ($featureId) {
		case Feature::DESKTOP:
			if ($this->isSelected(Feature::MATE)) {
				$this->configureFeature(Feature::NUMIX);
			}
			break;
		}
	}

	public function installFeature($featureId) {
		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::installFeature($featureId)'\n");
		}

		$propagate = TRUE;

		switch ($featureId) {
		case Feature::CONSOLE:
			$this->installPackages('dracut-network chrony ed xz zip unzip p7zip zstd rhash');
			$this->installPackages('rsync inetutils-ftp inetutils-traceroute bind-utils pwgen mkpasswd');
			$this->installPackages('lsof iotop iptraf-ng iftop nethogs nmap rsyslog neovim python3-pip python3-devel');

			if (!$this->isSelected(Feature::VM_INSTALL)) {
				//$this->installPackages('ipmitool smartmontools');
				//$this->installPackages('linux-headers dkms');
				$this->installPackages('fuse-exfat exfat-utils ntfs-3g');
				$this->installPackages('ufw ufw-extras gpgme cifs-utils');
				$this->installPackages('wol void-release-keys outils PopCorn nss-mdns');
			}

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installPackages('aspell-en aspell-de aspell-fr');
			}
			break;

		case Feature::DESKTOP:
			$this->installPackages('xorg-minimal xorg-input-drivers xorg-video-drivers');
			$this->installPackages('xorg-server-xephyr xorg-server-xnest xf86-video-qxl');
			$this->installPackages('exiftool xdg-user-dirs setxkbmap xinput xsel xorg-apps');
			$this->installPackages('ttf-bitstream-vera font-libertine-ttf liberation-fonts-ttf');
			// elogind polkit dbus-elogind
			$this->executeScriptlet(VoidLinuxInfo::ID . '/desktop');

			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installPackages('mesa-dri vulkan-loader mesa-vulkan-intel intel-video-accel');
				$this->installPackages('bluez-cups bluez-alsa broadcom-bt-firmware NetworkManager');
				$this->installPackages('alsa-plugins alsa-plugins-ffmpeg alsa-plugins-samplerate alsa-plugins-speex alsa-utils');
				$this->installPackages('lame libavresample pulseaudio alsa-plugins-pulseaudio');
				$this->installPackages('sane xiccd pdfgrep qpdf mtpfs brightnessctl');
				$this->installPackages('cups cups-filters foomatic-db cups-pk-helper');
				
				$this->installPackages('qemu bridge-utils vde2');
				$this->installArchive(Location::LOCAL_FOLDER, 'packages/ovmf-2021.02', '${INSTALL_PREFIX}/share');
				
				// Needed by gparted / partitionmanager
				$this->installPackages('btrfs-progs f2fs-tools mtools jfsutils cryptsetup lvm2 reiser4progs reiserfsprogs udftools nilfs-utils xfsprogs xfsdump hfsutils hfsprogs');
			}

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/10-keyboard_ch-fr.conf', '${CONFIG_PREFIX}/X11/xorg.conf.d');
			}

			if ($this->isSelected(Feature::MATE)) {
				//$this->installPackages('font-misc-misc terminus-font');
				// mate-extra in Void brings much less stuff than in Debian (only mate-calc is unneeded).
				$this->installPackages('mate mate-extra');
				$this->installPackages('gnome-keyring adwaita-icon-theme seahorse');
				$this->installPackages('galculator pluma eom');
				$this->installPackages('caja-open-terminal caja-sendto geany geany-plugins');

				if (!$this->isSelected(Feature::VM_INSTALL)) {
					$this->installPackages('network-manager-applet blueman gst-libav gufw');
					$this->installPackages('gvfs-cdda gvfs-smb gvfs-mtp gvfs-afc dconf-editor gparted');
					$this->installPackages('transmission-gtk gnome-disk-utility hexchat liferea');
					$this->installPackages('system-config-printer gscan2pdf');
				}

				//$this->installPackages('onboard orca xzoom');
				
				// Void provides numix-themes = numix-gtk-theme, but not numix-icon-theme
				$this->installFeature(Feature::NUMIX);
			}

			if ($this->isSelected(Feature::LXQT)) {
				$this->installPackages('lxqt');
				$this->installPackages('fbreader xpdf FeatherPad xcalc kColorPicker ksnip pavucontrol-qt');
				
				if (!$this->isSelected(Feature::VM_INSTALL)) {
					$this->installPackages('quassel qbittorrent cutecom kadu quiterss');
					$this->installPackages('nm-tray xscreensaver gvfs-cdda gvfs-smb gvfs-mtp gvfs-afc blueman');
				}
				
				// Void provides numix-themes = numix-gtk-theme, but not numix-icon-theme
				$this->installFeature(Feature::NUMIX);
			}

			if ($this->isSelected(Feature::KDE)) {
				$this->installPackages('kde5 kde5-baseapps xdg-desktop-portal xdg-desktop-portal-kde');
				$this->installPackages('gwenview okular kcalc kgpg ark kcron kmag kompare');
				$this->installPackages('kcolorchooser kolourpaint spectacle kimageformats');
				$this->installPackages('krfb krdc falkon filelight kruler kcharselect');
				$this->installPackages('kwalletmanager krename ksystemlog');
//				$this->installPackages('digikam'); // = darktable
//				$this->installPackages('kdenlive');
				
				if (!$this->isSelected(Feature::VM_INSTALL)) {
					$this->installPackages('konversation ktorrent print-manager partitionmanager skanlite');
				}
			}
			break;

		case Feature::MFC_L2710DW:
			// Printer driver
			$this->installPackages('brother-brlaser');
			// Scanner driver
			$this->installPackages('void-repo-nonfree');
			$this->executeCommand('xbps-install -Sy');
			$this->installPackages('brother-brscan4');
			break;

		case Feature::ET2700:
			// Printer driver
			$this->installPackages('epson-inkjet-printer-escpr');
			// Scanner driver
			$this->installPackages('imagescan imagescan-plugin-networkscan');
			break;

		case Feature::VNC:
			$this->installPackages('tigervnc');
			break;

		case Feature::XRDP:
			$this->executeCommand('echo "XRDP is not available on Void" 1>&2');
			break;

		case Feature::SAMBA:
			$this->installPackages('samba');
			break;

		case Feature::DISPLAY_MANAGER:
			if ($this->isSelected(Feature::LXDM)) {
				$this->installPackages('lxdm');
			} else if ($this->isSelected(Feature::SDDM)) {
				$this->installPackages('sddm');
			} else if ($this->isSelected(Feature::LIGHTDM)) {
				$this->installPackages('lightdm-gtk3-greeter');
			}
			break;

		case Feature::OFFICE_SUITE:
			$this->installPackages('libreoffice-writer libreoffice-calc libreoffice-impress libreoffice-math libreoffice-xtensions libreoffice-i18n-en-US');
			$this->installPackages('hunspell-en_US');
			$this->installPackages('gimp');
			$this->installPackages('inkscape');

			if ($this->isSelected(Feature::MATE)) {
				$this->installPackages('libreoffice-gnome');
			}

			if ($this->isSelected(Feature::KDE)) {
				$this->installPackages('libreoffice-kde');
			}

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installPackages('libreoffice-i18n-en-GB libreoffice-i18n-fr libreoffice-i18n-de libreoffice-i18n-lb');
				$this->installPackages('hunspell-en_GB-ize hunspell-de_DE hunspell-fr_FR-toutesvariantes');
			}
			break;

		case Feature::FIREFOX:
			switch ($this->getFirefoxVersion()) {
			case Feature::FIREFOX_ESR:
				$this->installPackages('firefox-esr firefox-esr-i18n-en-US');

				if ($this->isSelected(Feature::LUXEMBOURG)) {
					$this->installPackages('firefox-esr-i18n-en-GB firefox-esr-i18n-fr firefox-esr-i18n-de');
				}
				break;

			case Feature::FIREFOX_LATEST:
				$this->installPackages('firefox firefox-i18n-en-US');

				if ($this->isSelected(Feature::LUXEMBOURG)) {
					$this->installPackages('firefox-i18n-en-GB firefox-i18n-fr firefox-i18n-de');
				}
				break;
			}
			break;

		case Feature::THUNDERBIRD:
			$this->installPackages('thunderbird thunderbird-i18n-en-US');

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->installPackages('thunderbird-i18n-en-GB thunderbird-i18n-fr thunderbird-i18n-de');
			}
			break;

		case Feature::IM_CLIENT:
			$this->installPackages('pidgin pidgin-libnotify pidgin-otr pidgin-sipe libpurple-hangouts libpurple-skypeweb libpurple-telegram');
			break;

		case 'foliate':
			$this->installPackages('foliate');
			$this->installConfig(Location::LOCAL_FOLDER, 'desktop/com.github.johnfactotum.Foliate.desktop', '${APP_LAUNCHERS_DIR}');
			$this->installFile(Location::LOCAL_FOLDER, 'icons/com.github.johnfactotum.Foliate.svg', '${APP_ICONS_DIR}');
			break;

		case Feature::CALIBRE:
			$this->installPackages('calibre python-html2text python3-html2text');
			$propagate = FALSE;
			break;

		case Feature::NODEJS:
			$this->installPackages('nodejs');
			break;

		case Feature::OPENJDK:
			$this->installPackages('openjdk8 openjdk8-src openjdk8-doc');
			$this->installPackages('openjdk11 openjdk11-src openjdk11-doc');
			break;

		case Feature::ECLIPSE:
			$this->installPackages('eclipse');
			break;

		case Feature::VSCODE:
			$this->installPackages('vscode');
			break;

		case Feature::MYSQL:
			if ($this->isSelected(Feature::KDE)) {
				// MariaDB is up-to-date, but is incompatible with KDE :(
				// However, MySQL is available in version 5.6 :(
				$this->installPackages('mysql');
			} else {
				$this->installPackages('mariadb');
			}

			# Same service name for MySQL and MariaDB
			$this->executeCommand('enableService mysqld');
			$this->executeCommand('startService mysqld');
			break;

		case Feature::POSTGRESQL:
			// Note: the postgresql12-client package contains command-line utilities
			// (e.g. createdb, psql). They are installed in /usr/lib/psql12/bin.
			// Upon installation, /usr/lib/psql12/bin is not in PATH, but a scriptlet
			// in /etc/profile.d will take care of this upon next login. In the
			// meantime, command names must be fully qualified.
			$pgVersion = 13;
			$this->installPackages('postgresql' . $pgVersion . ' postgresql' . $pgVersion . '-client');

			if ($this->isSelected(Feature::DEVELOP)) {
				$this->installPackages('postgresql' . $pgVersion . '-doc postgresql' . $pgVersion . '-contrib pgadmin3');
				$this->installFile(Location::LOCAL_FOLDER, 'icons/postgresql.svg', '${APP_ICONS_DIR}');
				$this->installConfig(Location::LOCAL_FOLDER, 'desktop/pgadmin3.desktop', '${APP_LAUNCHERS_DIR}');
			}

			$this->executeCommand('usermod -d /var/lib/postgresql' . $pgVersion . ' -s /bin/bash postgres');
			$this->executeCommand('su postgres -c "/usr/lib/psql' . $pgVersion . '/bin/initdb -D /var/lib/postgresql' . $pgVersion . '"');
			$this->executeCommand('enableService postgresql' . $pgVersion);
			$this->executeCommand('startService postgresql' . $pgVersion);
			// Allow more time to PostgreSQL on its first start.
			// Otherwise, the first utilities invocations would fail,
			// not finding /tmp/s.PGSQL.5432 yet.
			$this->executeCommand('sleep 10');
			break;

		case Feature::SCM:
			if (!$this->isSelected(Feature::VM_INSTALL)) {
				$this->installPackages('cvs subversion mercurial');
			}

			$this->installPackages('git');
			break;

		case Feature::TCL_TK:
			$this->installPackages('tcl tk');
			break;

		case Feature::CMAKE:
			$this->installPackages('cmake cmake-gui meson');
			$this->installFile(Location::LOCAL_FOLDER, 'icons/cmake.svg', '${APP_ICONS_DIR}', 'CMakeSetup.svg');
			break;

		case Feature::DEVELOP:
			if (!$this->isSelected(Feature::VM_INSTALL)) {
				// Needed when using xbps-src
				$this->installPackages('curl bsdtar xtools');
			}

			$this->installPackages('man-pages-posix patch gdb zeal');
			$this->installPackages('clang clang-tools-extra lldb');
			$this->installPackages('codeblocks cscope cppcheck valgrind');
			//$this->executeScriptlet(VoidLinuxInfo::ID . '/freebsd-compatibility');

			if ($this->isSelected(Feature::KDE) && !$this->isSelected(Feature::VM_INSTALL)) {
				$this->installPackages('kdevelop kdevelop-php kdevelop-python kdevelop-pg-qt');
			}
			break;

		case Feature::APACHE_PHP:
			$this->installPackages('apache php-apache');
			$this->installPackages('php-pgsql php-xml php-mbstring php-gd php-zip');
			$this->installPackages('php-imagick php-intl php-pear php-sodium php-apcu php-sqlite php-tidy php-xsl xdebug php-phpdbg');
			break;

		case Feature::VBOX_HOST:
			// virtualbox-ose doesn't work correctly,
			// using virtualbox-ose-dkms instead is REQUIRED
			$this->installPackages('linux-headers dkms virtualbox-ose-dkms');
			break;

		case Feature::VBOX_GUEST:
			// virtualbox-ose-guest doesn't work correctly,
			// using virtualbox-ose-guest-dkms instead is REQUIRED
			$this->installPackages('linux-headers dkms virtualbox-ose-guest-dkms');
			break;

		case Feature::VMWARE_GUEST:
			$this->installPackages('open-vm-tools');
			$this->executeCommand('enableService vmtoolsd');
			break;

		case Feature::QEMU_GUEST:
			$this->installPackages('qemu-ga');
			$this->executeCommand('enableService qemu-ga');
			break;
		}

		if ($propagate) {
			parent::installFeature($featureId);
		}
	}

	public function completeFeature($featureId) {
		parent::completeFeature($featureId);

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::completeFeature($featureId)'\n");
		}

		switch ($featureId) {
		case Feature::CONSOLE:
			$this->executeScriptlet(VoidLinuxInfo::ID . '/console');

			if ($this->isSelected(Feature::LUXEMBOURG)) {
				$this->executeScriptlet(VoidLinuxInfo::ID . '/luxembourg');
			}
			break;

		case Feature::DESKTOP:
			if ($this->isSelected(Feature::MATE)) {
				$this->completeFeature(Feature::NUMIX);
			}

			$this->executeScriptlet(VoidLinuxInfo::ID . '/desktop');
			
			if ($this->isSelected(Feature::KDE)) {
				$this->executeCommand("echo 'export GTK_USE_PORTAL=1' >> /etc/profile.d/local.sh");
			}
			break;

		case Feature::DISPLAY_MANAGER:
			if ($this->isSelected(Feature::LIGHTDM)) {
				// Additional configuration needed on top of BaseUnixInstaller's
				$this->executeScriptlet(VoidLinuxInfo::ID . '/lightdm');
			}
			break;
		}
	}

	public function completeInstallation() {
		parent::completeInstallation();

		if ($this->debugMode) {
			print("echo '--- " . get_class() . "::completeInstallation()'\n");
		}

		$this->executeScriptlet(VoidLinuxInfo::ID . '/90-finish');
	}
}
