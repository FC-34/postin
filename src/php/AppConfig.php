<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

require_once 'choices/CountryChoice.php';

class AppConfig {
	private function __construct() {
		$this->installers = array();
		$this->countries = array();
		$this->postinHomeDir = null;
		$this->postinBaseUrl = null;
		$this->fileSearchPaths = array();
		$this->resourceSearchPaths = array();
		$this->customTitle = null;
		$this->customSubTitle = null;
		$this->defaultCountry = null;
		$this->logo = null;
		$this->javaPrefix = '/local';
		$this->loginBackground = 'INSTALL_PREFIX/share/backgrounds/vegetal-dreams/leaves/leaves0.png';
		$this->desktopBackground = 'INSTALL_PREFIX/share/backgrounds/vegetal-dreams/leaves/leaves0.png';
		$this->baseUrl = null;
		$this->serverRoot = null;
		$this->initialized = FALSE;
	}
	
	private static $instance = null;
	
	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new AppConfig();
		}
		
		return self::$instance;
	}
	
	public $installers;
	public $countries;
	public $fileSearchPaths;
	public $resourceSearchPaths;
	public $logo;
	public $customTitle;
	public $customSubTitle;
	public $defaultCountry;
	public $javaPrefix;
	// Display Manager background image
	public $loginBackground;
	// Desktop Environment default background image
	public $desktopBackground;
	public $baseUrl;
	public $serverRoot;
	private $initialized;
	
	// When implementing a derived post-installer with base and derived 
	// installed alongside (instead of nested):
	
	// postinHome = directory containing the base installer.
	public $postinHomeDir;
	// postinHome = base URL of the base installer resources.
	public $postinBaseUrl;
	
	public function setParent($parentDir, $parentUrl) {
		if ($parentDir !== null && $parentUrl !== null) {
			$this->postinHomeDir = $parentDir;
			$this->postinBaseUrl = $parentUrl;
		}
	}

	private function initializeSearchPaths() {
		if ($this->initialized) {
			return;
		}
		
		$scriptBaseDir = dirname($_SERVER['SCRIPT_FILENAME']);
		$scriptBaseUri = $_SERVER['REQUEST_URI'];
		$i = strpos($scriptBaseUri, '?');

		if ($i !== FALSE) {
			$scriptBaseUri = substr($scriptBaseUri, 0, $i);
		}
		
		if (substr($scriptBaseUri, strlen($scriptBaseUri) - 1, 1) === '/') {
			$scriptBaseUri = substr($scriptBaseUri, 0, strlen($scriptBaseUri) - 1);
		}
		
		foreach (explode(PATH_SEPARATOR, get_include_path()) as $path) {
			if (substr($path, 0, 1) !== '.' && is_dir($path . '/php/installers')) {
				// $path also contains files/ and sh/ => suitable
				// as file and resource search path.
				array_push($this->fileSearchPaths, $path);
				
				$urlCorrection = '';
				
				if ($path === $this->postinHomeDir && $this->postinBaseUrl !== null) {
					$urlCorrection = $this->postinBaseUrl;
					
					if (substr($urlCorrection, strlen($urlCorrection) - 1, 1) !== '/') {
						$urlCorrection = $urlCorrection . '/';
					}
				} else if ($path !== $scriptBaseDir) {
					$firstDiff = strspn($scriptBaseDir ^ $path, "\0");
					// Multibyte version, if ever needed (also replace substr/strlen with mb_substr/mb_strlen below):
					// $firstDiff = mb_strlen(mb_strcut($indexPath, 0, strspn($indexPath ^ $controllerPath, "\0")));
					$currentPathRemainder = substr($path, $firstDiff);
					$indexPathRemainder = substr($scriptBaseDir, $firstDiff);
					
					if (strlen($currentPathRemainder) === 0) {
						$levels = substr_count($indexPathRemainder, DIRECTORY_SEPARATOR);
						
						for ($i = 0; $i < $levels; $i++) {
							$urlCorrection = $urlCorrection . '../';
						}
					} else if (strlen($indexPathRemainder) === 0) {
						$urlCorrection = substr($currentPathRemainder, 1);
					} else {
						$levels = substr_count($indexPathRemainder, DIRECTORY_SEPARATOR);
						
						for ($i = 0; $i < $levels; $i++) {
							$urlCorrection = $urlCorrection . '../';
						}
						
						$urlCorrection = $urlCorrection . $currentPathRemainder;
					}
					
					if (DIRECTORY_SEPARATOR !== '/') {
						$urlCorrection = str_replace(DIRECTORY_SEPARATOR, '/', $urlCorrection);
					}
					
					if (substr($urlCorrection, strlen($urlCorrection) - 1, 1) !== '/') {
						$urlCorrection = $urlCorrection . '/';
					}
				}
				// else => $path === $scriptBaseDir => DO NOT append /
				
				array_push($this->resourceSearchPaths, $urlCorrection);
			}
		}
		
		$this->getBaseUrl();
		$this->initialized = TRUE;
	}
	
	public function locateFile($relPath) {
		$this->initializeSearchPaths();
		
		foreach ($this->fileSearchPaths as $path) {
			$filePath = $path . DIRECTORY_SEPARATOR . $relPath;
			
			if (is_file($filePath)) {
				return $filePath;
			}
		}
		
		return null;
	}

	public function locateResource($relPath) {
		$this->initializeSearchPaths();
		$n = count($this->fileSearchPaths);
		
		for ($i = 0; $i < $n; $i++) {
			$filePath = $this->fileSearchPaths[$i] . DIRECTORY_SEPARATOR . $relPath;
			
			if (is_file($filePath)) {
				$resourcePath = $this->resourceSearchPaths[$i] . $relPath;
				
				if (substr($resourcePath, 0, 1) === '/') {
					$resourcePath = $this->serverRoot . $resourcePath;
				} else {
					$resourcePath = $this->baseUrl . '/' . $resourcePath;
				}
				
				return $resourcePath;
			}
		}
		
		return null;
	}

	public function loadHtml($pageName) {
		$fileName = $this->locateFile($pageName);
		$html = $fileName != null ? file_get_contents($fileName) : '';
		$i = strpos($html, '<body');
		
		if ($i !== FALSE) {
			while (substr($html, $i++, 1) !== '>');
			
			$j = strpos($html, '</body>');
			
			if ($j !== FALSE) {
				$html = trim(substr($html, $i, $j - $i));
			} else {
				$html = trim(substr($html, $i));
			}
		}
		
		return $html;
	}
	
	public function addCountry($featureId, $description) {
		array_push($this->countries, new CountryChoice($featureId, $description));
		
		return $featureId;
	}
	
	public function registerInstaller($installer) {
		array_push($this->installers, $installer);
		
		return $installer;
	}
	
	public function getInstallerById($installerId) {
		foreach ($this->installers as $installer) {
			if ($installer->getId() === $installerId) {
				return $installer;
			}
		}
		
		return null;
	}

	public function getParameters() {
		$parameters = array();

		switch($_SERVER['REQUEST_METHOD']) {
		case 'POST':
			$parameters = $_POST;
			break;

		case 'GET':
			$parameters = $_GET;
			break;
		}

		return $parameters;
	}

	public function getPageUrl() {
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' 
			? 'https' 
			: 'http';
		$serverPort = $_SERVER['SERVER_PORT'];
		$urlPort = ($protocol == 'http' && $serverPort == 80) || ($protocol == 'https' && $serverPort == 443) 
			? '' 
			: (':' . $serverPort);
		$host = isset($_SERVER['HTTP_X_FORWARDED_HOST']) 
			? $_SERVER['HTTP_X_FORWARDED_HOST'] 
			: (isset($_SERVER['HTTP_HOST']) 
				? $_SERVER['HTTP_HOST'] 
				: $_SERVER['SERVER_NAME']);

		return $protocol . '://' . $host . $urlPort . $_SERVER['REQUEST_URI'];
	}

	public function getBaseUrl() {
		if ($this->baseUrl === null) {
			$baseUrl = $this->getPageUrl();
			$i = strpos($baseUrl, '?');

			if ($i !== FALSE) {
				$baseUrl = substr($baseUrl, 0, $i);
			}
			
			if (substr($baseUrl, strlen($baseUrl) - 1, 1) === '/') {
				$baseUrl = substr($baseUrl, 0, strlen($baseUrl) - 1);
			}
			
			$this->baseUrl = $baseUrl;
			$i = strpos($baseUrl, '/', strpos($baseUrl, '://') + 3);

			if ($i !== FALSE) {
				$this->serverRoot = substr($baseUrl, 0, $i);
			} else {
				$this->serverRoot = $baseUrl;
			}
		}
		
		return $this->baseUrl;
	}
	
	public function getServerAddress() {
		return $_SERVER['SERVER_ADDR'];
	}
	
	public function isServerAddressInternal() {
		$serverIp = $this->getServerAddress();
		$firstDot = strpos($serverIp, '.');
		$secondDot = $firstDot !== FALSE ? strpos($serverIp, '.', $firstDot + 1) : FALSE;

		if ($firstDot !== FALSE && $firstDot !== FALSE) {
			$firstByte = intval(substr($serverIp, 0, $firstDot));
			$secondByte = intval(substr($serverIp, $firstDot + 1, $secondDot - $firstDot - 1));
			return 
				$firstByte === 127 
				|| $firstByte === 10 
				|| ($firstByte === 172 && $secondByte >= 16 && $secondByte <= 31)
				|| ($firstByte === 192 && $secondByte === 168);
		} else {
			return strpos($serverIp, 'fe80:') === 0
				|| strpos($serverIp, '2001:7e8:cc03:b700:') === 0;
		}
	}
}
