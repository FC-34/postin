<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

function dispatch() {
	global $appConfig;

	$selectedInstaller = null;
	$selectedProfile = null;
	$selectedFeatures = null;
	$featureList = '';
	$hasStep1 = TRUE;
	$hasStep2 = TRUE;
	$installerOk = TRUE;
	$errorMessage = null;
	$installationInstructions = '';
	$installerIntroduction = array();
	$viewName = null;
	$parameters = $appConfig->getParameters();

	if (count($appConfig->installers) === 1) {
		// Auto-select installer when only one available (=> skip step 1)
		$selectedInstaller = $appConfig->installers[0];
		$hasStep1 = FALSE;
	} else {
		// Check parameters to determine selected installer
		if (array_key_exists('installer', $parameters)) {
			$selectedInstaller = $appConfig->getInstallerById($parameters['installer']);
		}
	}

	if ($selectedInstaller !== null) {
		// Check parameters to determine selected usage profile
		// (no => entering step 2, yes => entering step 3)
		if (array_key_exists('profile', $parameters)) {
			$selectedProfile = $parameters['profile'];
		} else if (count($selectedInstaller->features) == 0 && count($appConfig->countries) == 1) {
			// Auto-select usage profile when only one available
			// and no optional feature proposed (=> skip step 2)
			switch (count($selectedInstaller->profiles)) {
			case 1:
				// Profile 'none' = add features to existing system
				$selectedProfile = $selectedInstaller->profiles[0]->id;
				$hasStep2 = FALSE;
				break;
				
			case 2:
				// Auto-select first "non-none" profile
				$selectedProfile = $selectedInstaller->profiles[1]->id;
				$hasStep2 = FALSE;
				break;
			}
			
			$parameters['country'] = $appConfig->countries[0]->id;
		}
	}

	// Check parameters to determine requested features
	// (no => entering step 3, yes => entering step 4)
	if (array_key_exists('features', $parameters)) {
		$selectedFeatures = explode(',', $parameters['features']);
	}

	// Process events
	if ($selectedInstaller !== null) {
		if ($selectedFeatures !== null) {
			// Step 4: generate script
		} else if ($selectedProfile !== null) {
			// Step 3: display installation instructions
			$viewName = 'display-instructions';
			$selectedFeatures = array();
			
			// Translate profile into features
			foreach ($selectedInstaller->getProfileById($selectedProfile)->features as $featureId) {
				array_push($selectedFeatures, $featureId);
			}
			
			// Add country, display manager and desktop environment
			if (array_key_exists('country', $parameters)) {
				array_push($selectedFeatures, $parameters['country']);
			}
			
			if (in_array(Feature::DESKTOP, $selectedFeatures)) {
				if (array_key_exists('dm', $parameters)) {
					$featureId = $parameters['dm'];
					
					if ($featureId !== 'none') {
						array_push($selectedFeatures, $featureId);
					}
				}

				if (array_key_exists('de', $parameters)) {
					$featureId = $parameters['de'];
					
					if ($featureId !== 'none') {
						array_push($selectedFeatures, $featureId);
					}
				}
			}
			
			// Add optional features
			foreach ($parameters as $name => $value) {
				if ($value === 'feature') {
					array_push($selectedFeatures, $name);
				}
			}
			
			$featureList = implode(',', $selectedFeatures);
			
			// Load installation instructions if provided
			$installationInstructions = 'php/instructions/' . $selectedInstaller->getId() . '.php';
		} else {
			// Step 2: select usage profile and optional features
			$viewName = 'select-features';
		}
	} else {
		// Step 1: select installer
		$viewName = 'select-installer';
		
		// Collect data for installer selector
		foreach ($appConfig->installers as $installer) {
			// Load introduction text if provided
			$installerIntroduction[$installer->getId()] = $appConfig->loadHtml('php/introduction/' . $installer->getId() . '.html');
		}
	}

	// Generate output
	header('Cache-Control: no-cache,no-store');
	header('Expires: 0');
	header('Pragma: no-cache');
	
	if ($viewName !== null) {
		header('Content-Type: text/html');
		require $appConfig->locateFile('php/views/' . $viewName . '.php');
	} else {
		header('Content-Type: text/plain');
		$selectedInstaller->generateScript($appConfig, $selectedFeatures);
	}
}
