<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

class Feature {
	const INSTALLER = 'installer';
	const INITIAL_INSTALL = 'initial-install';
	const CONSOLE = 'console';
	const DESKTOP = 'desktop';
	const DESKTOP_BASE = 'desktop-base';
	const DESKTOP_COMMON = 'desktop-common';
	const DISPLAY_MANAGER = 'display-manager';
	const DEVELOP = 'develop';
	const LATEST = 'latest';
	const LUXEMBOURG = 'luxembourg';
	const MATE = 'mate';
	const KDE = 'kde';
	const LXQT = 'lxqt';
	const XDM = 'xdm';
	const LXDM = 'lxdm';
	const SDDM = 'sddm';
	const LIGHTDM = 'lightdm';
	const FIREFOX = 'firefox';
	const FIREFOX_ESR = 'firefox-esr';
	const FIREFOX_LATEST = 'firefox-latest';
	const KONTAKT = 'kontakt';
	const THUNDERBIRD = 'thunderbird';
	const OFFICE_SUITE = 'office-suite';
	const MEDIA_PLAYER = 'media-player';
	const POSTGRESQL = 'postgresql';
	const SCM = 'scm';
	const TCL_TK = 'tcl-tk';
	const CMAKE = 'cmake';
	const VNC = 'vnc';
	const XRDP = 'xrdp';
	const XINETD = 'xinetd';
	const SAMBA = 'samba';
	const MYSQL = 'mysql';
	const NUMIX = 'numix';
	const ECLIPSE = 'eclipse';
	const NODEJS = 'nodejs';
	const VSCODE = 'vscode';
	const APACHE_PHP = 'apache-php';
	const OPENJDK = 'openjdk';
	const VBOX_HOST = 'vbox-host';
	const VBOX_GUEST = 'vbox-guest';
	const VMWARE_GUEST = 'vmware-guest';
	const QEMU_GUEST = 'qemu-guest';
	const IM_CLIENT = 'pidgin';
	const CALIBRE = 'calibre';
	const SOAPUI = 'soapui';
	const SQL_WORKBENCH = 'sql-workbench';
	// Disables functionalities not found in a VM (e.g. Bluetooth)
	const VM_INSTALL = 'vm-install';
	// Brother Laser MFP
	const MFC_L2710DW = 'mfc-l2710dw';
	// Epson Inkjet MFP
	const ET2700 = 'et2700';

	public static function describe($value) {
		if (is_array($value)) {
			$result = '[ ';

			foreach ($value as $item) {
				if (strlen($result) > 2) {
					$result = $result . ', ';
				}

				$result = $result . self::describe($item);
			}

			$result = $result . ' ]';

			return $result;
		} else {
			return str_replace('-', '_', strtoupper($value));
		}
	}
		
	public static function getName($id) {
		if ($id === Feature::LXQT) {
			return 'LXQt';
		} else if ($id === Feature::KDE) {
			return 'KDE5';
		} else if ($id === Feature::LIGHTDM) {
			return 'LightDM';
		} else if ($id === Feature::XDM) {
			return 'XDM / XenoDM';
		} else if ($id === 'none') {
			return 'None';
		}
		
		return mb_strtoupper($id);
	}
}
