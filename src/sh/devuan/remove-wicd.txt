# Install network-manager-gnome beforehand if your purpose is
# to replace wicd with network-manager.

stopService wicd
removePackages wicd wicd-gtk wicd-daemon python-wicd rfkill
rm -f /etc/xdg/autostart/wicd-tray.desktop

# After removal of wicd, network-manager needs to be restarted
# to recover network connectivity.

# restartService network-manager
