# When using Debian, Eclipse has to be downloaded from eclipse.org,
# the package from Debian's repository is antediluvian.

#ECLIPSE_MIRROR=http://mirror.dkm.cz
ECLIPSE_MIRROR=http://ftp-stud.fht-esslingen.de/pub/Mirrors

# Available releases:
#
# TARGET Comment
# --------------------------
# R      Release
# M1     Milestone 1
# M2     
# M3     
# RC1    Release Candidate 1
# RC2    
# ...
ECLIPSE_RELEASE=R

# Available targets:
#
# TARGET
# --------------------------
# linux-gtk-x86_64.tar.gz
# macosx-cocoa-x86_64.dmg
# win32-x86_64.zip
ECLIPSE_TARGET=linux-gtk-x86_64.tar.gz

# Available downloads:
#
# BUNDLE
# ----------
# committers
# cpp       
# dsl       
# java      
# javascript
# jee       
# modeling  
# parallel  
# php       
# rcp       
# rust      
# scout     
# testing   
ECLIPSE_BUNDLE=jee

eclipseVersionIsRelease() {
    # When a release is available, the version's directory listing contains an entry in the form:
    # <tr><td valign="top"><img src="/icons/folder.gif" alt="[DIR]"></td><td><a href="R/">R/</a></td><td align="right">2020-06-15 23:02  </td><td align="right">  - </td><td>&nbsp;</td></tr>
    local dirEntry="$(catURL ${ECLIPSE_MIRROR}/eclipse/technology/epp/downloads/release/$1/ | grep -F href=\"${ECLIPSE_RELEASE}/\")"
    
    if [ -n "${dirEntry}" ]; then
        return 0
    else
        return 1
    fi
}

eclipseLastRelease() {
    # Version numbers are in the form YYYY-MM. Version directory listing uses the following format:
    # <tr><td valign="top"><img src="/icons/folder.gif" alt="[DIR]"></td><td><a href="2020-06/">2020-06/</a></td><td align="right">2020-07-11 03:38  </td><td align="right">  - </td><td>&nbsp;</td></tr>
    # A version directory is created as soon as a first milestone build is available.
    local versions="$(catURL ${ECLIPSE_MIRROR}/eclipse/technology/epp/downloads/release/ | grep -E href=[\"][[:digit:]]{4}-[[:digit:]]{2} | cut -d ] -f 2 | cut -d \" -f 3 | cut -d / -f 1 | sort -r)"
    local lastVersion="$(echo "${versions}" | head -n 1)"
    
    if eclipseVersionIsRelease "${lastVersion}"; then
        echo -n "${lastVersion}"
    else
        echo -n "$(echo "${versions}" | head -n 2 | tail -n 1)"
    fi
}

eclipseArchiveName() {
    local ECLIPSE_VERSION=$1
    echo -n "$(catURL ${ECLIPSE_MIRROR}/eclipse/technology/epp/downloads/release/${ECLIPSE_VERSION}/${ECLIPSE_RELEASE}/ | grep \"eclipse-${ECLIPSE_BUNDLE}-.*-${ECLIPSE_TARGET}\" | cut -d ] -f 2 | cut -d \" -f 3)"
}

eclipseURL() {
    local ECLIPSE_VERSION=$(eclipseLastRelease)
    local ECLIPSE_ARCHIVE=$(eclipseArchiveName ${ECLIPSE_VERSION})
    echo -n "${ECLIPSE_MIRROR}/eclipse/technology/epp/downloads/release/${ECLIPSE_VERSION}/${ECLIPSE_RELEASE}/${ECLIPSE_ARCHIVE}"
}
