cleanPackages

if [ -d ${JAVA_PREFIX}/share/applications ]; then
    cp -f ${JAVA_PREFIX}/share/applications/*.desktop ${APP_LAUNCHERS_DIR}/
    cp -f ${JAVA_PREFIX}/share/pixmaps/* ${APP_ICONS_DIR}/
    update-desktop-database ${APP_LAUNCHERS_DIR}
fi

if [ -d ${INSTALL_PREFIX}/share/glib-2.0/schemas ]; then
    glib-compile-schemas ${INSTALL_PREFIX}/share/glib-2.0/schemas
fi

# Update existing users' startup scripts (remember this is a post-
# installation script and none of them has been customized yet).
# ======================================================================

for d in $(cut -d : -f 6 /etc/passwd | sort -u); do
    if [ -d ${d} ]; then
        for f in .cshrc .profile .shrc; do
            if [ -f /etc/skel/${f} -a -f ${d}/${f} ]; then
                cp -f /etc/skel/${f} ${d}/${f}
            fi
        done
    fi
done

firstUser="$(getFirstUser)"

if [ -n "${firstUser}" ]; then
    if [ -d /home/${firstUser}/.config/dconf ]; then
        # Force reinitialisation of desktop configuration
        rm -rf /home/${firstUser}/.config/dconf
    fi

    if [ -d /etc/skel/.config ]; then
        # Copy the DE's default configuration to the first user,
        # as it was created before running the installation script.
        cp -r /etc/skel/.config /home/${firstUser}/
        chown -R ${firstUser}:${firstUser} /home/${firstUser}/.config
    fi
fi
