if [ -d ${INSTALL_PREFIX}/share/icons/Numix ]; then
    gtk-update-icon-cache ${INSTALL_PREFIX}/share/icons/Numix
fi

if [ -d ${INSTALL_PREFIX}/share/icons/Numix-Light ]; then
    gtk-update-icon-cache ${INSTALL_PREFIX}/share/icons/Numix-Light
fi
