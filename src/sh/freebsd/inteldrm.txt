# Determine port to build
case "$(osGrade)" in
CURRENT)
    drmPortName="drm-current-kmod"
    ;;
*)
    drmPortName="drm-fbsd$(osMajorVersion).0-kmod"
    ;;
esac

INTERIM_INSTALL=

if [ -n "${INTERIM_INSTALL}" ]; then
    if [ "$(osFullVersion)" = '12.1' ]; then
        installArchive files ${FILES_DIR}/freebsd/drm-fbsd12.0-kmod-4.16.g20200221 / txz
        installArchive files ${FILES_DIR}/freebsd/gpu-firmware-kmod-g20200130 / txz
        kldxref /boot/modules
    elif [ -d /usr/ports/graphics/${drmPortName} ]; then
        checkSourcesAndTools
        cd /usr/ports/graphics/${drmPortName}
        make install clean
        pkg lock -y ${drmPortName}
        pkg lock -y gpu-firmware-kmod
    fi
else
    installPackages ${drmPortName}
fi

if [ -f /boot/modules/i915kms.ko ]; then
    psysrc kld_list += /boot/modules/i915kms.ko

    # Works around a bug on laptops (hdac0: command timeout on address 2)
    cat <<EOF >> /boot/loader.conf
compat.linuxkpi.i915_disable_power_well="0"
EOF
fi

