# Set correct script permissions
if [ -f /local/etc/X11/xdm/Xsetup_0 ]; then
    chmod +x /local/etc/X11/xdm/Xsetup_0
fi

if [ -f /local/etc/X11/xdm/Xsession ]; then
    chmod +x /local/etc/X11/xdm/Xsession
fi

# Backup original XDM configuration
cp -f /etc/X11/xdm/xdm-config /etc/X11/xdm/xdm-config.orig

# Install customisations
chmod u+w /etc/X11/xdm/xdm-config

# Use custom XDM setup script (e.g. to set background image)
${SED_CMD} 's|^\(DisplayManager._0.setup:\).*|\1/local/etc/X11/xdm/Xsetup_0|' /etc/X11/xdm/xdm-config

# Use custom X resources
${SED_CMD} 's|^\(DisplayManager\*resources:[[:space:]]*\).*|\1/local/etc/X11/xdm/Xresources|' /etc/X11/xdm/xdm-config

# Use custom XDM session script, executed by a login shell
${SED_CMD} 's|^\(DisplayManager\*session:[[:space:]]*\).*|\1/bin/ksh -l /local/etc/X11/xdm/Xsession|' /etc/X11/xdm/xdm-config

chmod u-w /etc/X11/xdm/xdm-config

# Enable XDM service
enableService xdm
