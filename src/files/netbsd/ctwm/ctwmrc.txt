# File locations -------------------------------------------------------

# Window Decorations
IconDirectory "INSTALL_PREFIX/share/themes/Numix/openbox-3"

# Other Images
PixmapDirectory "INSTALL_PREFIX/share/themes/Numix/openbox-3:INSTALL_PREFIX/share/pixmaps:INSTALL_PREFIX/share/icons"

# Optimizations

NoBackingStore
NoSaveUnders

# Cursor Styles --------------------------------------------------------

Cursors {
	Frame	"top_left_arrow"
	Title	"top_left_arrow"
	Icon	"top_left_arrow"
	IconMgr	"top_left_arrow"
	Move	"fleur"
	Resize	"sizing"
	Menu	"top_left_arrow"
	Button	"top_left_arrow"
	Wait	"watch"
	Select	"dot"
	Destroy	"pirate"
}

# Window Decorations ---------------------------------------------------

Color {
	BorderColor "#f06860"

	# Titlebars
	TitleBackground "#444444"
	TitleForeground "#eeeeee"

	# sizing and information windows
	DefaultBackground "#444444"
	DefaultForeground "#eeeeee"
}

Monochrome {
	BorderColor "white"

	# Titlebars
	TitleBackground "black"
	TitleForeground "white"

	# sizing and information windows
	DefaultBackground "black"
	DefaultForeground "white"
}

#ResizeFont "fixed"
#TitleFont "variable"
TitleJustification "center"
ButtonIndent 0
TitleButtonBorderWidth 0
TitlePadding 0
BorderWidth 2
NoTitleHighlight

LeftTitleButton "menu.xbm" = f.menu "WindowMenu"
RightTitleButton "iconify.xbm" = f.iconify
RightTitleButton "max.xbm" = f.fullzoom
RightTitleButton "close.xbm" = f.deleteordestroy

# Non-iconified Windows ------------------------------------------------

NoDefaults
ClickToFocus
RaiseOnClick
RandomPlacement
BorderResizeCursors
OpaqueMove
OpaqueResize
AutoOccupy
UsePPosition "on"
IconifyByUnmapping
DontMoveOff
DontSqueezeTitle

# Transient Windows ----------------------------------------------------

CenterFeedbackWindow
DecorateTransients
TransientHasOccupation
AutoFocusToTransients

# Window List (aka. Icon Manager) --------------------------------------

Color {
	IconManagerBackground "#444444"
	IconManagerForeground "#eeeeee"
	IconManagerHighlight "#f06860"
}

Monochrome {
	IconManagerBackground "black"
	IconManagerForeground "white"
	IconManagerHighlight "white"
}

#IconManagerFont "variable"
NoCaseSensitive
NoIconManagerFocus
BorderBottom 32
IconManagerGeometry "720x32+0-0" 9
ShowIconManager

AlwaysOnTop {
	"TWM Icon Manager"
}

NoTitle {
	"TWM Icon Manager"
}

# Window Cycling (aka. Warp) -------------------------------------------

WindowRing
WarpRingOnScreen
WarpUnmapped
ShortAllWindowsMenus

# Workspace Manager ----------------------------------------------------

Color {
	MapWindowBackground "#eeeeee"
	MapWindowForeground "#f06860"
}

Monochrome {
	MapWindowBackground "white"
	MapWindowForeground "black"
}

MapWindowCurrentWorkSpace { "#f06860" "#eeeeee" "#444444" }
MapWindowDefaultWorkSpace { "#eeeeee" "#444444" "#eeeeee" }

#WorkSpaceFont ""
WorkSpaceManagerGeometry "256x32-0-0" 4

WorkSpaces {
	# name [{bg-button [fg-button] [bg-root] [fg-root] [pixmap-root]}]
	"1"
	"2"
	"3"
	"4"
}

DontPaintRootWindow
ShowWorkSpaceManager
StartInMapState
ReallyMoveInWorkspaceManager
DontToggleWorkSpaceManagerState
DontWarpCursorInWMap

AlwaysOnTop {
	"WorkSpaceManager"
}

NoTitle {
	"WorkSpaceManager"
}

# Menus ----------------------------------------------------------------

Color {
	MenuBackground "#444444"
	MenuForeground "#eeeeee"
	MenuTitleBackground "#444444"
	MenuTitleForeground "#eeeeee"
}

Monochrome {
	MenuBackground "black"
	MenuForeground "white"
	MenuTitleBackground "black"
	MenuTitleForeground "white"
}

#MenuFont "variable"
NoMenuShadows
StayUpMenus
WarpToDefaultMenuEntry
IgnoreCaseInMenuSelection

menu "Applications" {
	"Calculator" f.exec "xcalc&"
	"Clock" f.exec "xclock&"
	"Eyes" f.exec "xeyes&"
	"X Logo" f.exec "xlogo&"
	"Magnifying Glass" f.exec "xmag&"
	"Manual" f.exec "xman&"
	"Terminal" f.exec "xterm&"
}

menu "Desktop" {
	"Restart WM" f.restart
	"Show Desktop" f.showbackground
	"Refresh Windows" f.refresh
	"Kill Window" f.exec "xkill&"
}

menu "Session" {
	"Lock Screen" f.exec "(sleep 1;xlock -mode blank)&"
	"Quit" f.quit
}

menu "RootMenu" {
	"Applications" f.menu "Applications"
	"Desktop" f.menu "Desktop"
	"Session" f.menu "Session"
}

menu "WindowMenu" {
	"Move" f.move
	"Resize" f.resize
	"Minimize" f.iconify
	"Maximize" f.fullzoom
	"" f.separator
	"Properties" f.identify
	"Refresh" f.winrefresh
	"Fit to Content" f.fittocontent
	"" f.separator
	"Show on all workspaces" f.occupyall
	"Remove from workspace" f.vanish
	"" f.separator
	"Close" f.deleteordestroy
}

# Function definitions -------------------------------------------------

Function "move-or-raise" { f.move f.deltastop f.raise }
Function "raise-and-focus" { f.deiconify f.raise f.focus }

# Key bindings ---------------------------------------------------------

IgnoreLockModifier

# Modifiers = shift, control, lock, meta, mod1, mod2, mod3, mod4, mod5
# Contexts = root, window, icon, iconmgr, frame, title, workspace / all

"Tab" = meta : all : f.warpring "next"
"F4" = meta : window : f.delete
"Left" = meta : all : f.prevworkspace
"Right" = meta : all : f.nextworkspace
"Super_L" = : all : f.showbackground

# Mouse bindings -------------------------------------------------------

Button1 = : root : f.menu "RootMenu"
Button1 = : title|icon : f.function "move-or-raise"
Button1 = : iconmgr : f.function "raise-and-focus"
Button1 = control|meta : window : f.deleteordestroy
