<?php
/*
SPDX-License-Identifier: BSD-2-Clause

Copyright (c) 2018-2020, Vincent DEFERT, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

/*
 * It is required to define the include_path using relative paths. 
 * They will be used by AppConfig to locate both files and resources.
 * Absolute paths will be ignored.
 * 
 * And, of course, set_include_path() must be called before
 * AppConfig::getInstance().
 */

$baseDir = dirname($_SERVER['SCRIPT_FILENAME']);

set_include_path(
	$baseDir . '/php' . PATH_SEPARATOR . $baseDir . PATH_SEPARATOR .
	get_include_path()
);

require_once 'installers/Debian.php';
require_once 'installers/Devuan.php';
require_once 'installers/FreeBSD.php';
require_once 'installers/DragonFlyBSD.php';
require_once 'installers/NetBSD.php';
require_once 'installers/OpenBSD.php';
require_once 'installers/VoidLinux.php';
require_once 'AppConfig.php';
require_once 'Controller.php';

$appConfig = AppConfig::getInstance();

$appConfig->registerInstaller(new VoidLinux());
$appConfig->registerInstaller(new NetBSD());
$appConfig->registerInstaller(new OpenBSD());
$appConfig->registerInstaller(new Devuan());
$appConfig->registerInstaller(new Debian());
$appConfig->registerInstaller(new DragonFlyBSD());
$appConfig->registerInstaller(new FreeBSD());

$appConfig->addCountry(Feature::LUXEMBOURG, 'Grand-Duchy of Luxembourg');
$appConfig->addCountry('usa', 'United States of America');

// The following properties may be set to override the default
// title and subtitle in all pages.
//$appConfig->customTitle = null;
//$appConfig->customSubTitle = null;

// The following properties may be set to define the featureId
// of the country to initially select in the dropdown list.
//$appConfig->defaultCountry = null;

dispatch();
